﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public Room currentRoom;
    public List<KeyTypes> currentKeys = new List<KeyTypes>();
    [HideInInspector]
    public ScreenShakeData currentScreenShake;
    public static Player instance = null;
    public ModuleConnector doorEntered;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }
    public float timelastClick;
    public bool _debug;
    // Update is called once per frame
    GameObject lastObject;
    //void FixedUpdate()
    //{
    //    //RaycastHit hit;
    //    //if (Input.GetMouseButtonDown(0))
    //    //{
    //    //    if (Physics.Raycast(transform.position, transform.forward, out hit, 3f))
    //    //    {
    //    //        if (hit.collider.GetComponentInParent<Door>())
    //    //            hit.collider.GetComponentInParent<Door>().ClickedDoor();
    //    //    }

    //    //}
    //    //var mainCamera = FindCamera();
    //    //if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition).origin, mainCamera.ScreenPointToRay(Input.mousePosition).direction, out hit, 100, Physics.DefaultRaycastLayers))
    //    //{
    //    //    if (hit.collider.tag == "MemoryGameShpere")
    //    //    {
    //    //        if (Input.GetMouseButtonDown(0))
    //    //        {
    //    //            //print(Time.time - timelastClick);
    //    //            if (Time.time - timelastClick < 0.25f)
    //    //                return;
    //    //            timelastClick = Time.time;
    //    //            StartCoroutine(SetColorForTime(hit.collider.gameObject, 1f));
    //    //        }
    //    //        else
    //    //        {
    //    //            if (hit.collider.GetComponent<Renderer>().material.GetFloat("_RimPower") != 0.25f)
    //    //            {
    //    //                hit.collider.GetComponent<Renderer>().material.SetFloat("_RimPower", 1f);
    //    //                lastObject = hit.collider.gameObject;
    //    //            }
    //    //        }
    //    //    }
    //    //    else
    //    //    {
    //    //        if (lastObject)
    //    //        {
    //    //            if (lastObject.GetComponent<Renderer>().material.GetFloat("_RimPower") != 0.25f)
    //    //            {
    //    //                lastObject.GetComponent<Renderer>().material.SetFloat("_RimPower", 6f);
    //    //            }
    //    //        }
    //    //    }
    //    //}
    //    if (_debug)
    //        Debug.DrawRay(transform.position, transform.forward, Color.red, 3f);
    //}
    IEnumerator SetColorForTime(GameObject obj, float time)
    {
        obj.GetComponent<Renderer>().material.SetFloat("_RimPower", 0.25f);
        var spheres = obj.GetComponentInParent<MemoryGame>().spheres;
        for (var i = 0; i < spheres.Length; i++)
        {
            if (spheres[i].gameObject.GetInstanceID() == obj.gameObject.GetInstanceID())
            {
                obj.GetComponentInParent<MemoryGame>().AddAttempt(i);
            }
        }
        //obj.GetComponentInParent<MemoryGame>().clickedSpheres.Add()
        yield return new WaitForSeconds(time);
        obj.GetComponent<Renderer>().material.SetFloat("_RimPower", 6f);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Room>())
        {
            currentRoom = other.GetComponent<Room>();
            doorEntered = currentRoom.connectorSpawnedOn;
        }

        /* if(other.GetComponent<Room>() && other.GetComponent<Room>().Tags[0] == "MemoryRoom")
         {
             currentRoom.GetComponentInChildren<MemoryGame>().walls[0].SetActive(true);
             currentRoom.GetComponentInChildren<MemoryGame>().walls[1].SetActive(true);
             currentRoom.CloseAllDoors();
             currentRoom.GetComponentInChildren<MemoryGame>().ReplyGlow();
         }*/
        /*
        if (other.tag == "Trap")
        {
            Destroy(Player.instance.gameObject);
        }*/
    }

    public Camera FindCamera()
    {
        if (GetComponentInChildren<Camera>())
        {
            return GetComponentInChildren<Camera>();
        }

        return Camera.main;
    }
}
