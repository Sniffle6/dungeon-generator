﻿using UnityEngine;
using System.Linq;
using UnityEditor;
using System.Collections;

public abstract class Module : MonoBehaviour
{
    public bool hasBeenSeen;
    protected bool spawnNew;
    public ModuleConnector connectorSpawnedOn;
    [Tooltip("The name of this module")]
    public string[] Tags;
   public bool ignoreThisOne;
    public GameObject[] geometry;
    public ModuleConnector[] GetExits
    {
        get { if (_childConnectors.Length == 0) { return _childConnectors = GetComponentsInChildren<ModuleConnector>(); } else return _childConnectors; }
    }
    ModuleConnector[] _childConnectors = new ModuleConnector[] { };
    public ModuleConnector[] GetExitsWithTag(string tagToMatch)
    {
        var exits = GetComponentsInChildren<ModuleConnector>();
        return exits.Where(e => e.ContainsTag(tagToMatch)).ToArray();
    }
    public void GenerateNewRoom(ModuleConnector connectorToSpawnOn)
    {
        var newTag = Misc.GetRandom(connectorToSpawnOn.Tags);
        var newModulePrefab = Misc.GetRandomWithTag(ModuleGenerator.instance._availableRooms, newTag);
        var newModule = (Module)Instantiate(newModulePrefab);
        newModule.gameObject.name = "" + newModulePrefab.name + " " + ModuleGenerator.instance._existingRooms.Count.ToString("n0");
        var newModuleExits = newModule.GetExits;
        //var exitToMatch = newModuleExits.FirstOrDefault(x => x.IsDefault) ?? Misc.GetRandom(newModuleExits);
        ModuleConnector exitToMatch;
        var exitsToMatch = newModuleExits.Where(x => x.IsDefault).ToArray();
        //print(exitsToMatch.Length);
        if (exitsToMatch.Length > 0)
            exitToMatch = Misc.GetRandom(exitsToMatch);
        else
            exitToMatch = Misc.GetRandom(newModuleExits);
        MatchExits(connectorToSpawnOn, exitToMatch);
        // print("center: "+newModule.GetComponent<Collider>().bounds.center);
        var overlapped = Physics.OverlapBox(newModule.GetComponent<Collider>().bounds.center, newModule.GetComponent<Collider>().bounds.extents);
        foreach (var obj in overlapped)
        {
            if (obj.GetComponent<Room>() && newModule.gameObject.GetInstanceID() != obj.gameObject.GetInstanceID() && obj.gameObject.GetInstanceID() != connectorToSpawnOn.GetComponentInParent<Room>().gameObject.GetInstanceID())
            {
                print(newModule.gameObject.name + " : " + obj.name);
                Destroy(newModule.gameObject);
                //GenerateWall(connectorToSpawnOn);
                return;
            }
        }


        ModuleGenerator.instance._existingRooms.Add(newModule);
        newModule.connectorSpawnedOn = connectorToSpawnOn;
        newModule.hasBeenSeen = false;
        connectorToSpawnOn.connectedRoom = exitToMatch;
        exitToMatch.connectedRoom = connectorToSpawnOn;
        connectorToSpawnOn.hasConnected = true;
        exitToMatch.hasConnected = true;
        ModuleGenerator.instance.timeSinceLastSpawn = Time.time;
        //newExits.AddRange(newModuleExits.Where(e => e != exitToMatch));
    }
    public void GenerateWall(ModuleConnector connectorToSpawnOn)
    {
        var clone = Resources.Load("Modules/Wall") as GameObject;
        var wall = Instantiate(clone);
        var mod = wall.GetComponent<Module>();
        var newModuleExits = mod.GetExits;
        var exitToMatch = Misc.GetRandom(newModuleExits);
        MatchExits(connectorToSpawnOn, exitToMatch);
       // ModuleGenerator.instance._existingRooms.Add(mod);
        mod.connectorSpawnedOn = connectorToSpawnOn;
        connectorToSpawnOn.connectedRoom = exitToMatch;
        exitToMatch.connectedRoom = connectorToSpawnOn;
        connectorToSpawnOn.hasConnected = true;
        exitToMatch.hasConnected = true;
        ModuleGenerator.instance.timeSinceLastSpawn = Time.time;
    }
    private void MatchExits(ModuleConnector oldExit, ModuleConnector newExit)
    {//
        var newModule = newExit.transform.parent;
        var forwardVectorToMatch = -oldExit.transform.forward;
        var correctiveRotation = Misc.Azimuth(forwardVectorToMatch) - Misc.Azimuth(newExit.transform.forward);
        newModule.RotateAround(newExit.transform.position, Vector3.up, correctiveRotation);
        var correctiveTranslation = oldExit.transform.position - newExit.transform.position;
        newModule.transform.position += correctiveTranslation;
    }
   public bool destroyRoutineRunning;
    //IEnumerator DestroyRoutine()
    //{
    //    while (destroyRoutineRunning) {
    //        yield return new WaitForSeconds(10);
    //        if (!ignoreThisOne && !IsVisibleFrom(GetComponent<Collider>(), Camera.main)){
    //            var connectorFornewRoom = this.connectorSpawnedOn;
    //            ModuleGenerator.instance._existingRooms.Remove(this);
    //            DestroyImmediate(this.gameObject);
    //            GenerateNewRoom(connectorFornewRoom);
    //        }
    //        destroyRoutineRunning = false;
    //    }
    //}
    //void calculateShouldDestroy()
    //{
    //    if (this.GetType() != typeof(Room))
    //        return;
    //    if (!hasBeenSeen)
    //        return;
    //    if (!destroyRoutineRunning && !IsVisibleFrom(GetComponent<Collider>(), Camera.main))
    //    {
    //        destroyRoutineRunning = true;
    //        StartCoroutine(DestroyRoutine());
    //    }
    //}
    private void Update()
    {
       // calculateShouldDestroy();
        //if (!connectorSpawnedOn && gameObject.GetInstanceID() != Player.instance.currentRoom.gameObject.GetInstanceID())
        //{
        //    ModuleGenerator.instance._existingRooms.Remove(this);
        //    DestroyImmediate(this.gameObject);
        //}
    }
    public bool IsVisibleFrom(Collider col, Camera camera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, col.bounds);
    }
    private void OnDrawGizmos()
    {
            Gizmos.DrawWireCube(GetComponent<Collider>().bounds.center, GetComponent<Collider>().bounds.extents * 2);
    }
}
