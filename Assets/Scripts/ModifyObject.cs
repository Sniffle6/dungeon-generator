﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ModifyObject : MonoBehaviour
{

    //  public UnityEvent myEvent;
    public UnityEvent _Start;
    public UnityEvent _Update;
    public UnityEvent _OnTriggerEnter;
    public UnityEventWgameObject _OnPlayerClick;
    Camera p_Camera;
    public int[] eventCounts;
    // Use this for initialization
    void Start()
    {
        eventCounts = new int[5];
        eventCounts[0] = _Start.GetPersistentEventCount();
        eventCounts[1] = _Update.GetPersistentEventCount();
        eventCounts[2] = _OnPlayerClick.GetPersistentEventCount();
        eventCounts[3] = _OnTriggerEnter.GetPersistentEventCount();
        p_Camera = Player.instance.FindCamera();
        if (eventCounts[0] > 0)
            _Start.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        if (eventCounts[1] > 0)
            _Update.Invoke();
        //
    }
    private void FixedUpdate()
    {
        if (eventCounts[2] <= 0)
            return;
        RaycastHit hit;
        if (Physics.Raycast(p_Camera.ScreenPointToRay(Input.mousePosition).origin, p_Camera.ScreenPointToRay(Input.mousePosition).direction, out hit, 100, Physics.DefaultRaycastLayers))
        {
            if (Input.GetMouseButtonDown(0) && hit.collider.gameObject.GetInstanceID() == gameObject.GetInstanceID())
                _OnPlayerClick.Invoke(hit.collider.gameObject);
        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (eventCounts[3] > 0)
            _OnTriggerEnter.Invoke();
    }
    public void FlipActive(GameObject obj)
    {
        obj.SetActive(!obj.activeSelf);
    }
}
