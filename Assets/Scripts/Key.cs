﻿using UnityEngine;
using System.Collections;

public enum KeyTypes
{
    Key1
}
public class Key : MonoBehaviour {
    public KeyTypes keyType;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")){
            other.GetComponent<Player>().currentKeys.Add(keyType);
            var con = GetComponentInChildren<ModuleConnector>();
            con.connectedRoom.hasConnected = false;
            Destroy(gameObject);
        }
    }
}
