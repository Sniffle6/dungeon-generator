﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncrementObject : MonoBehaviour {

    float lerpTime = 1f;
    public float currentLerpTime;

    float moveDistance = 0.15f;

    Vector3 startPos;
    Vector3 endPos;
    public bool MovePosition;
     Vector3 direction;
    // Use this for initialization
    void Start ()
    {
        direction = (transform.parent.position - transform.position).normalized;
        startPos = transform.position;
        endPos = transform.position + direction * moveDistance;
    }
	
	// Update is called once per frame
	void Update () {
        MoveWalls();
    }
    void MoveWalls()
    {
        if (MovePosition) {
            //increment timer once per frame
            currentLerpTime += Time.deltaTime*0.25f;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
            }
            float perc = currentLerpTime / lerpTime;
            transform.position = Vector3.Lerp(startPos, endPos, perc);
            if (perc == 1)
            {
                startPos = transform.position;
                endPos = transform.position + direction * moveDistance;
                currentLerpTime = 0f;
                MovePosition = false;
            }
        }
    }
}
