﻿using UnityEngine;
using System.Collections;

public class LockDoors : MonoBehaviour
{
    public bool lockAllDoors;
    public KeyTypes requiredKey;
    public string[] doorTagsToLock;
    // Use this for initialization
    void Start()
    {
        var exits = transform.root.GetComponent<Module>().GetExits;
        for (var i = 0; i < exits.Length; i++)
        {
            if (exits[i].connectedRoom && exits[i].connectedRoom.GetComponentInParent<Door>())
            {
                exits[i].connectedRoom.GetComponentInParent<Door>().requiredKey = requiredKey;
                exits[i].connectedRoom.GetComponentInParent<Door>().locked = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
