﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.SceneManagement;


public class Door : Module
{
    public KeyTypes requiredKey;
    public bool locked;
    public bool test;
    // Use this for initialization
    void Start()
    {
      //  locked = false;
    }

    public ModuleConnector GetEmptyConnector
    {
        get
        {
            var siblings = GetComponentsInChildren<ModuleConnector>();
            return siblings.FirstOrDefault(m => m.connectedRoom == null) ?? null;
        }
    }
    public ModuleConnector GetClosestConnector
    {
        get
        {
            var siblings = GetComponentsInChildren<ModuleConnector>();
            float distance = float.MaxValue;
            ModuleConnector connectorToReturn = null;
            for (var i = 0; i < siblings.Length; i++)
            {
                if (Vector3.Distance(siblings[i].transform.position, Player.instance.transform.position) < distance)
                {
                    distance = Vector3.Distance(siblings[i].transform.position, Player.instance.transform.position);
                    connectorToReturn = siblings[i];
                }

            }
            return connectorToReturn;
        }
    }
    public ModuleConnector GetFurthestConnector
    {
        get
        {
            var siblings = GetComponentsInChildren<ModuleConnector>();
            float distance = 0;
            ModuleConnector connectorToReturn = null;
            for (var i = 0; i < siblings.Length; i++)
            {
                if (Vector3.Distance(siblings[i].transform.position, Player.instance.transform.position) > distance)
                {
                    distance = Vector3.Distance(siblings[i].transform.position, Player.instance.transform.position);
                    connectorToReturn = siblings[i];
                }

            }
            return connectorToReturn;
        }
    }
    public void ClickedDoor()
    {
        if (Tags[0].Contains("LoadScene"))
        {
            print("loading");
            SceneManager.LoadScene(2);
            return;
        }
        if (GetEmptyConnector)
        {
            var keys = Player.instance.currentKeys;
            for (var i = 0; i < keys.Count; i++)
            {
                if (keys[i] == requiredKey)
                {
                    locked = false;
                    Player.instance.currentKeys.Remove(keys[i]);
                }
            }
            if (locked)
            {
                print("locked");
                return;
            }
        }
        else
        {
            var playerExits = Player.instance.currentRoom.GetExits;
            //print(playerExits.Length);
            for (var i = 0; i < playerExits.Length; i++)
            {
                if (playerExits[i].connectedRoom && playerExits[i].connectedRoom.OppositeConnector)
                    playerExits[i].connectedRoom.OppositeConnector.hasConnected = false;
            }
        }
    }
}
