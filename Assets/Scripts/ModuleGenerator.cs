﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ModuleGenerator : MonoBehaviour
{
    public Module[] _availableRooms;
    public List<Module> _existingRooms = new List<Module>();
    public Player p_Player;
    public Player player;
    public int roomsToSpawn = 2;
    public Module wall;
    public static ModuleGenerator instance = null;
    public float distanceToCull;
    public bool cullRooms;
    public float timeSinceLastSpawn;
    void Awake()
    {
        _availableRooms = Resources.LoadAll("Modules", typeof(Module)).Cast<Module>().ToArray();
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {
        //PickFirstRoom();
        if (!player)
            player = Instantiate(p_Player);
        var respawn = GameObject.FindGameObjectWithTag("Respawn");
        player.transform.position = respawn.transform.position;
    }

    private void PickFirstRoom()
    {
        var starterModels = _availableRooms.Where(m => m.GetType() == typeof(Room)).ToArray();
        var _startModule = starterModels[Random.Range(0, starterModels.Length)].GetComponent<Module>();
        var clone = _startModule;
        var firstRoom = Instantiate(clone);
        firstRoom.transform.position = transform.position;
        _existingRooms.Add(firstRoom.GetComponent<Module>());
    }
    public float distance = 5.0f;
    public float theAngle = 45;
    public float segments = 10;
    public float offset;
    bool RaycastSweep(int objInstanceId)
    {
        var startPos = player.transform.GetChild(0).transform.position; // umm, start position !
        var targetPos = Vector3.zero; // variable for calculated end position

        var startAngle = -theAngle * 0.5; // half the angle to the Left of the forward
        var finishAngle = theAngle * 0.5; // half the angle to the Right of the forward

        // the gap between each ray (increment)
        float inc = theAngle / segments;

        RaycastHit hit;

        // step through and find each target point
        for (float i = (float)startAngle; i < finishAngle; i += inc) // Angle from forward
        {
            targetPos = (Quaternion.Euler(0, i, 0) * player.transform.GetChild(0).forward).normalized * distance;

            // linecast between points
            if (Physics.Linecast(startPos, targetPos, out hit))
            {
                if (objInstanceId == hit.collider.gameObject.GetInstanceID())
                {
                    Debug.Log("Hit " + hit.collider.gameObject.name);
                    return true;
                }
            }

            // to show ray just for testing
            Debug.DrawLine(startPos, targetPos, Color.green);
        }
        return false;
    }
    // Update is called once per frame
    void Update()
    {
        //print(Time.time + "<- time | Time - timeLastSpawn = " + (Time.time - timeSinceLastSpawn));
        if (cullRooms == false && _existingRooms.Count > roomsToSpawn || Time.time - timeSinceLastSpawn >= 1.5f)
        {
           // cullRooms = true;
        }
        if (!cullRooms)
            return;
        for (var i = 0; i < _existingRooms.Count; i++)
        {
            if (Vector3.Distance(_existingRooms[i].transform.position, player.transform.position) > distanceToCull)
            {
                var renderers = _existingRooms[i].geometry;
                for (int e = 0; e < renderers.Length; e++)
                {
                    if (renderers[e].activeSelf)
                        renderers[e].SetActive(false);
                }
                continue;
            }
            if (IsVisibleFromWithScale(_existingRooms[i].GetComponent<Collider>(), Camera.main))
            {
                if (cullRooms)
                {
                    var renderers = _existingRooms[i].geometry;
                    for (int e = 0; e < renderers.Length; e++)
                    {
                        if (!renderers[e].activeSelf)
                            renderers[e].SetActive(true);
                    }
                    //_existingRooms[i].gameObject.SetActive(true);
                }
                _existingRooms[i].hasBeenSeen = true;
                //  if (!RaycastSweep(_existingRooms[i].gameObject.GetInstanceID()))
                //  {
                //      _existingRooms[i].gameObject.SetActive(false);
                // }
            }
            else
            {
                if (cullRooms)
                {
                    var renderers = _existingRooms[i].geometry;
                    for (int e = 0; e < renderers.Length; e++)
                    {
                        if (renderers[e].activeSelf)
                            renderers[e].SetActive(false);
                    }
                    // _existingRooms[i].gameObject.SetActive(false);
                }
            }
        }

    }
    public bool IsVisibleFromWithScale(Collider col, Camera camera)
    {
        var oldBound = col.bounds;
        oldBound.Expand(new Vector3(3f, 3f, 3f));
        var newBound = oldBound;
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, newBound);
    }
    public bool IsVisibleFrom(Collider col, Camera camera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, col.bounds);
    }
}
