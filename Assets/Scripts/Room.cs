﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Room : Module
{
    // Use this for initialization
    public Module[] availableRooms = new Module[] { };
    Renderer[] renderers;
    void Start()
    {
        // print(ModuleGenerator.instance._availableRooms.Length);
        for (var i = 0; i < GetExits.Length; i++)
        {
            if (ModuleGenerator.instance._existingRooms.Count >= ModuleGenerator.instance.roomsToSpawn)
            {
                if (!GetExits[i].connectedRoom)
                {
                    GenerateWall(GetExits[i]);
                }
            }
            else
            {
                if (!GetExits[i].connectedRoom)
                    GenerateNewRoom(GetExits[i]);
            }
        }
        renderers = GetComponentsInChildren<Renderer>();
    }
    public ModuleConnector[] GetLights
    {
        get
        {
            var exits = GetComponentsInChildren<ModuleConnector>();
            var listToReturn = new List<ModuleConnector>();
            for (var i = 0; i < exits.Length; i++)
            {
                if (exits[i].ContainsTag("Light") || exits[i].ContainsTag("light"))
                {
                    listToReturn.Add(exits[i]);
                }
            }
            return listToReturn.ToArray();
        }
    }
}
