﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Difficulty
{
    Easy,
    Medium,
    Hard
}
public class MemoryGame : MonoBehaviour
{

    public GameObject[] spheres;
    Renderer rend;
    Material mat;
    bool corutinePlaying;
    [SerializeField]
    List<int> clickOrder = new List<int>();
    [SerializeField]
    Difficulty difficulty;
    [SerializeField]
    int ammountToGuess;
    public GameObject[] baseOfObject;
    public List<int> clickedSpheres = new List<int>();
    bool replay = false;
    public GameObject[] walls;
    public ModifyObjectType onLoss;
    public void AddAttempt(int attempt)
    {
        clickedSpheres.Add(attempt);
        var keepMoving = false;
        if (clickedSpheres.Count >= clickOrder.Count)
        {
            print("same length");
            for (var i = 0; i < clickOrder.Count; i++)
            {
                if (clickOrder[i] == clickedSpheres[i])
                {
                    print("Correct");
                    keepMoving = true;
                }
                else
                {
                    print("Wrong");
                    keepMoving = false;
                    break;
                }

            }
            if (keepMoving)
            {
                walls[0].SetActive(false);
                walls[1].SetActive(false);
            }
            else
            {
                print("lost");
                replay = true;
                clickedSpheres.Clear();
                StartCoroutine(Replay());
                walls[0].GetComponent<IncrementObject>().currentLerpTime = 0;
                walls[0].GetComponent<IncrementObject>().MovePosition = true;
                walls[1].GetComponent<IncrementObject>().currentLerpTime = 0;
                walls[1].GetComponent<IncrementObject>().MovePosition = true;
                onLoss.Initialize();
                //Player.instance.FindCamera().GetComponent<ScreenShake>().testNormal = true;
            }
        }
    }
    // Use this for initialization
    public void StartGame()
    {
        var randDiff = Random.Range((int)0, (int)3);
        switch (randDiff)
        {
            case 0:
                difficulty = Difficulty.Easy;
                break;
            case 1:
                difficulty = Difficulty.Medium;
                break;
            case 2:
                difficulty = Difficulty.Hard;
                break;
        }
        switch (difficulty)
        {
            case Difficulty.Easy:
                ammountToGuess = 3;
                for (var i = 0; i < baseOfObject.Length; i++)
                {

                    baseOfObject[i].GetComponent<Renderer>().material.SetColor("_MKGlowTexColor", Color.green);
                    baseOfObject[i].GetComponent<Renderer>().material.SetColor("_MKGlowColor", Color.green);
                }
                break;
            case Difficulty.Medium:
                ammountToGuess = 5;
                for (var i = 0; i < baseOfObject.Length; i++)
                {
                    baseOfObject[i].GetComponent<Renderer>().material.SetColor("_MKGlowTexColor", Color.yellow);
                    baseOfObject[i].GetComponent<Renderer>().material.SetColor("_MKGlowColor", Color.yellow);
                }
                break;
            case Difficulty.Hard:
                ammountToGuess = 7;
                for (var i = 0; i < baseOfObject.Length; i++)
                {
                    baseOfObject[i].GetComponent<Renderer>().material.SetColor("_MKGlowTexColor", Color.red);
                    baseOfObject[i].GetComponent<Renderer>().material.SetColor("_MKGlowColor", Color.red);
                }
                break;
        }
        if (spheres.Length <= 0)
        {
            Debug.LogError("No spheres found");
            return;
        }
        corutinePlaying = true;
        StartCoroutine(SetGlow());
    }
    public void ReplyGlow()
    {
        replay = true;
        StartCoroutine(Replay());
    }
    IEnumerator Replay()
    {
        var i = 0;
        while (replay)
        {
            yield return new WaitForSeconds(2);
            if (i >= clickOrder.Count)
            {
                spheres[clickOrder[i - 1]].GetComponent<Renderer>().material.SetFloat("_RimPower", 6f);
                replay = false;
                yield break;
            }
            spheres[clickOrder[i]].GetComponent<Renderer>().material.SetFloat("_RimPower", 0.25f);
            if (i > 0)
                spheres[clickOrder[i - 1]].GetComponent<Renderer>().material.SetFloat("_RimPower", 6f);
            print(i);
            i++;
        }
    }
    IEnumerator SetGlow()
    {
        while (corutinePlaying)
        {
            var randomNumber = Random.Range(0, spheres.Length);
            var foundShpere = false;
            var attemptedNumbers = 0;
            if (clickOrder.Count >= ammountToGuess)
            {
                foundShpere = false;
                corutinePlaying = false;
                for (var i = 0; i < spheres.Length; i++)
                {
                    spheres[i].GetComponent<Renderer>().material.SetFloat("_RimPower", 6);
                }
                yield break;
            }
            while (!foundShpere)
            {
                if (spheres[randomNumber].GetComponent<Renderer>().material.GetFloat("_RimPower") > 5)
                {
                    foundShpere = true;
                }
                else
                {
                    attemptedNumbers++;
                    if (attemptedNumbers > 1000)
                    {
                        foundShpere = false;
                        corutinePlaying = false;
                        yield break;
                    }
                    randomNumber = Random.Range(0, spheres.Length);
                }
            }
            spheres[randomNumber].GetComponent<Renderer>().material.SetFloat("_RimPower", 0.25f);
            clickOrder.Add(randomNumber);
            for (var i = 0; i < spheres.Length; i++)
            {
                if (i != randomNumber)
                {
                    spheres[i].GetComponent<Renderer>().material.SetFloat("_RimPower", 6);
                }
            }
            yield return new WaitForSeconds(1);
        }
    }
}
