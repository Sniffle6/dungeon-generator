﻿using UnityEngine;
using System.Collections;

public class Prop : Module
{
    Color[] colors = new Color[] { new Color(0.5f,0,0), Color.black, new Color(0, 0, 0.5f), new Color(0, 0.5f, 0.5f), Color.gray, new Color(0f, 0.212f, 0f), new Color(0.5f, 0, 0.5f), new Color(0.5f, 0.6f, 0.1f) };
    void Start()
    {

        for (var i = 0; i < GetExits.Length; i++)
        {
            if (!GetExits[i].connectedRoom)
                GenerateNewRoom(GetExits[i]);
            else
                connectorSpawnedOn = GetExits[i];
        }

    }
}
