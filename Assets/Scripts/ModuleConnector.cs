﻿using UnityEngine;


public class ModuleConnector : MonoBehaviour
{
    public string[] Tags;
    public ModuleConnector connectedRoom;
    public bool hasConnected;
    public bool IsDefault;
    public bool testingProp;
    public GameObject testProp;
    [HideInInspector]
    public bool randomLightcolor;
    [HideInInspector]
    public float m_Intensity = 2.6f;
    [HideInInspector]
    public Color m_Color = Color.white;
    [HideInInspector]
    public float m_Range = 2.5f;
    [HideInInspector]
    public float m_Radius = 0.1f;
    [HideInInspector]
    public float m_Length = 2.82f;
    void Update()
    {
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        var scale = 1.0f;

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * scale);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position - transform.right * scale);
        Gizmos.DrawLine(transform.position, transform.position + transform.right * scale);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + Vector3.up * scale);

        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 0.025f);
    }
#endif
    public Module module
    {
        get { if (GetComponentInParent<Module>()) { return GetComponentInParent<Module>(); } return null; }
    }
    public ModuleConnector OppositeConnector
    {
        get
        {
            ModuleConnector connectorToReturn = null;
            for (var i = 0; i < siblings.Length; i++)
            {
                if (siblings[i])
                {
                    if (siblings[i].gameObject.GetInstanceID() != gameObject.GetInstanceID())
                    {
                        connectorToReturn = siblings[i];
                        return connectorToReturn;
                    }
                }
            }
            return connectorToReturn;
        }
        //return siblings.First(s => s.gameObject.GetInstanceID() != gameObject.GetInstanceID());
    }
    ModuleConnector[] _sibs = new ModuleConnector[] {};
    public ModuleConnector[] siblings
    {
        get { if (_sibs.Length == 0) { return _sibs = transform.parent.GetComponentsInChildren<ModuleConnector>(); } else { return _sibs; } }
    }
    public bool ContainsTag(string tagToMatch)
    {
        for (var i = 0; i < Tags.Length; i++)
        {
            if (Tags[i].Contains(tagToMatch))
            {
                return true;
            }
        }
        return false;
    }
}
