﻿using System.Collections;
using System;
using UnityEngine;

[Serializable]
public class rcData
{
    [SerializeField]
    public Light light;
    [SerializeField]
    public ModuleConnector door;
}
public class RoomCycle : MonoBehaviour
{
    public rcData[] lights;
    bool startCycle;
    bool pauseCycle;
    // Use this for initialization
    void Start()
    {
        startCycle = true;
        StartCoroutine(CycleLights());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator CycleLights()
    {
        int index = 0;
        while (startCycle)
        {
            if (pauseCycle)
            {
                yield return new WaitUntil(() => pauseCycle == false);
            }
            else
            {
                if (index == 0)
                {
                    for (var i = 0; i < lights.Length; i++)
                    {
                        lights[i].light.intensity = 0;
                        lights[i].door.connectedRoom.GetComponentInParent<Door>().locked = true;
                    }
                }
                else
                {
                    lights[index - 1].light.intensity = 0;
                    lights[index - 1].door.connectedRoom.GetComponentInParent<Door>().locked = true;
                }

                lights[index].light.intensity = 8;
                lights[index].door.connectedRoom.GetComponentInParent<Door>().locked = false;
                index++;
                if (index == 4)
                    index = 0;
            }
            yield return new WaitForSeconds(0.25f);
        }
    }
}
