﻿using UnityEngine;
using System;

public enum Direction
{
    Up,
    Down,
    Left,
    Right,
    Forward,
    Backward
}
[Serializable]
public class MoveChunk
{
    [SerializeField]
    public Direction _direction;
    [SerializeField]
    public float _unitsToMove;
    public float _speed;
}
public class MoveObject : MonoBehaviour
{
    [SerializeField]
    public MoveChunk[] directions;
    // bool movingBack;
    int currentItteration = 0;
    Vector3 lastWaypointPos = Vector3.zero;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.localPosition, lastWaypointPos) > directions[currentItteration]._unitsToMove)
        {

            if (currentItteration < directions.Length - 1)
                currentItteration++;
            else if (currentItteration >= directions.Length - 1)
                currentItteration = 0;
            lastWaypointPos = transform.localPosition;
        }
        if (currentItteration < directions.Length)
        {
            switch (directions[currentItteration]._direction)
            {
                case Direction.Up:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition + new Vector3(0, 1, 0), Time.deltaTime * directions[currentItteration]._speed);
                    break;
                case Direction.Down:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition + new Vector3(0, -1, 0), Time.deltaTime * directions[currentItteration]._speed);
                    break;
                case Direction.Left:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition + new Vector3(1, 0, 0), Time.deltaTime * directions[currentItteration]._speed);
                    break;
                case Direction.Right:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition + new Vector3(-1, 0, 0), Time.deltaTime * directions[currentItteration]._speed);
                    break;
                case Direction.Forward:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition + new Vector3(0, 0, 1), Time.deltaTime * directions[currentItteration]._speed);
                    break;
                case Direction.Backward:
                    transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition + new Vector3(0, 0, -1), Time.deltaTime * directions[currentItteration]._speed);
                    break;
            }
        }
    }
}
