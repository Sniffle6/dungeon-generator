﻿using UnityEngine;
using System.Collections;

public class DoorBehaviour : StateMachineBehaviour
{

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    // override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

    // }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!animator)
            return;
        //var lights = animator.transform.root.GetComponent<Door>().GetFurthestConnector.connectedRoom.GetComponentInParent<Room>().GetLights;
        //Debug.Log(lights.Length);
        //if (lights.Length <= 0)
        //    return;
        //if (stateInfo.normalizedTime >= 0.8f)
        //{
        //    for (var i = 0; i < lights.Length; i++)
        //    {
        //        if (lights[i] && lights[i].connectedRoom)
        //            lights[i].connectedRoom.transform.root.GetComponentInChildren<Light>().intensity--;
        //    }
        //}
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    //    var lights = animator.transform.root.GetComponent<Door>().GetFurthestConnector.connectedRoom.GetComponentInParent<Room>().GetLights;
    //    for (var i = 0; i < lights.Length; i++)
    //    {
    //        if (lights[i] && lights[i].connectedRoom)
    //            lights[i].connectedRoom.transform.root.GetComponentInChildren<Light>().intensity = 0;
    //    }
        Destroy(animator.transform.root.GetComponent<Door>().GetFurthestConnector.connectedRoom.module.gameObject);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here


    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
