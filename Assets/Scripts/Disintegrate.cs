﻿using UnityEngine;
using System.Collections;

public class Disintegrate : MonoBehaviour {
    Renderer rend;
    Material mat;
    public bool killCollider;
    Vector3 startPos;
	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();
        mat = rend.material;
        startPos = transform.position;


    }

    // Update is called once per frame
    float lastFramesHole=0;

    void Update ()
    {
        float holes = Mathf.PingPong(Time.time*0.5f, 1.0F);
        if(holes > lastFramesHole)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position-Vector3.up, Time.deltaTime);
        }
        else
        {
            transform.position = startPos;
        }
        lastFramesHole = holes;
        if (killCollider)
        {
            if (holes >= 0.9f && GetComponent<Collider>().enabled)
            {
                GetComponent<Collider>().enabled = false;
            }
            else if (holes <= 0.8f && !GetComponent<Collider>().enabled)
            {
                GetComponent<Collider>().enabled = true;
            }
        }
        mat.SetFloat("_DissolveAmount", holes);

    }
}
