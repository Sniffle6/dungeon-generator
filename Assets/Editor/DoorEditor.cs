﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Door))]
[CanEditMultipleObjects]
public class DoorEditor : Editor
{
    bool displayWarning;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Door door = (Door)target;
        var exits = door.GetComponentsInChildren<ModuleConnector>();
        if (exits.Length > 0)
        {
            for (var i = 0; i < exits.Length; i++)
            {
                if (exits[i] && exits[i].Tags != null)
                {
                    if (exits[i].Tags.Length == 0)
                    {
                        displayWarning = true;

                    }
                }
            }
        }
        if (displayWarning)
        {
            EditorGUILayout.HelpBox("Add tags to ModuleConnectors!", MessageType.Error);
        }
        if (PrefabUtility.GetPrefabParent(door.gameObject) == null)
        {
            EditorGUILayout.HelpBox("This object is not a prefeb!", MessageType.Error);
        }
        if (GUILayout.Button("Create Prefab"))
        {
            Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Resources/Modules/" + door.gameObject.name + ".prefab");
            PrefabUtility.ReplacePrefab(door.gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
        }
    }
}