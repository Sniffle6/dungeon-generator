﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Room))]
[CanEditMultipleObjects]
public class ModuleEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Room myScript = (Room)target;
        if (GUILayout.Button("Add Door"))
        {
            AddDoor(myScript);
        }
        if (GUILayout.Button("Add Prop"))
        {
            AddProp(myScript);
        }
        if (GUILayout.Button("Add Light"))
        {
            AddLight(myScript);
        }
        if (GUILayout.Button("Add Key"))
        {
            AddKey(myScript);
        }
        if (GUILayout.Button("Create Prefab"))
        {
            Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Resources/Modules/" + myScript.gameObject.name + ".prefab");
            PrefabUtility.ReplacePrefab(myScript.gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
        }
    }
    void AddDoor(Room room)
    {
        Debug.LogWarning("Dont forget to check tags!");
        var newObj = new GameObject("DoorConnector");
        newObj.transform.SetParent(room.gameObject.transform);
        newObj.transform.localPosition = Vector3.zero;
        var moduleCon = newObj.AddComponent<ModuleConnector>();
        moduleCon.IsDefault = true;
        string[] tagsTouse = new string[] { "Door", "Door1" };
        moduleCon.Tags = tagsTouse;
        Undo.RegisterCreatedObjectUndo(newObj, "Created DoorConnector");
        Selection.activeGameObject = newObj;
    }
    void AddLight(Room room)
    {
        Debug.LogWarning("Dont forget to check tags!");
        var newObj = new GameObject("LightConnector");
        newObj.transform.SetParent(room.gameObject.transform);
        newObj.transform.localPosition = Vector3.zero+new Vector3(0,2.68f,0);
        var moduleCon = newObj.AddComponent<ModuleConnector>();
        string[] tagsTouse = new string[] { "PointLight01", "PointLight02", "PointLight03", "PointLight04", "PointLight05", "PointLight06" };
        moduleCon.Tags = tagsTouse;
        Undo.RegisterCreatedObjectUndo(newObj, "Created LightConnector");
        Selection.activeGameObject = newObj;
    }
    void AddProp(Room room)
    {
        Debug.LogWarning("Dont forget to check tags!");
        var newObj = new GameObject("PropConnector");
        newObj.transform.SetParent(room.gameObject.transform);
        newObj.transform.localPosition = Vector3.zero;
        var moduleCon = newObj.AddComponent<ModuleConnector>();
        string[] tagsTouse = new string[] { "Rock01", "Sodacan", "Brick01", "Rock02", "Rock03", "Rock04" };
        moduleCon.Tags = tagsTouse;
        Undo.RegisterCreatedObjectUndo(newObj, "Created PropConnector");
        Selection.activeGameObject = newObj;
    }
    void AddKey(Room room)
    {
        Debug.LogWarning("Dont forget to check tags!");
        var newObj = new GameObject("KeyConnector");
        newObj.transform.SetParent(room.gameObject.transform);
        newObj.transform.localPosition = Vector3.zero;
        var moduleCon = newObj.AddComponent<ModuleConnector>();
        string[] tagsTouse = new string[] { "Key" };
        moduleCon.Tags = tagsTouse;
        Undo.RegisterCreatedObjectUndo(newObj, "Created KeyConnector");
        Selection.activeGameObject = newObj;
    }
}