﻿using UnityEngine;
using UnityEditor;

public class AddNewMenuItems
{
    [MenuItem("GameObject/Light/RT Tube Light", false, 0)]
    static void TubeLight()
    {
        Camera sceneCam = SceneView.lastActiveSceneView.camera;
        Vector3 spawnPos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
        GameObject t = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Plugins/TubeLight/TubeLight.prefab", typeof(GameObject));
        var g = GameObject.Instantiate(t);
        g.transform.position = spawnPos;
        Undo.RegisterCreatedObjectUndo(g, "Created Tube Light");
        Selection.activeGameObject = g;
        // t.transform.position = spawnPos;


    }
    [MenuItem("GameObject/Light/RT Area Light", false, 0)]
    static void AreaLight()
    {
        Camera sceneCam = SceneView.lastActiveSceneView.camera;
        Vector3 spawnPos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
        GameObject t = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Plugins/AreaLight/AreaLight.prefab", typeof(GameObject));
        var g = GameObject.Instantiate(t);
        g.transform.position = spawnPos;
        Undo.RegisterCreatedObjectUndo(g, "Created Area Light");
        Selection.activeGameObject = g;
    }
}