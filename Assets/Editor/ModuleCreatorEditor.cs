﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ModuleGenerator))]
[CanEditMultipleObjects]
public class ModuleCreatorEditor : Editor
{
    public string[] options = new string[] { "Door", "Room", "Prop", "Light" };
    public string[] lightTypes = new string[] { "Default Unity", "Tube", "Area" };
    public int index = 0;
    public int lightTypeIndex;
    Object selectedObject = null;
    bool openFromMissing;
    Color lightColor = Color.white;
    string nameField = "Enter Name...";
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GUILayout.Label("\n__________________________________\n");
        EditorGUILayout.HelpBox("Select a model with the same name as the model imported.", MessageType.Info);

        nameField = EditorGUILayout.TextField("Object Name: ", nameField);

        GUILayout.BeginHorizontal(GUILayout.Height(20));
        {
            GUILayout.FlexibleSpace();
            ModuleGenerator myScript = (ModuleGenerator)target;
            var button = GUILayout.Button("Create new Module", GUILayout.Width(Screen.width / 2), GUILayout.Height(25));
            index = EditorGUILayout.Popup(index, options, GUILayout.Width(Screen.width / 3));
            if (button)
            {
                switch (index)
                {
                    case 0:
                        Debug.LogWarning("Select a model with the same name as the model imported.");
                        if (!selectedObject)
                        {
                            openFromMissing = true;
                            EditorGUIUtility.ShowObjectPicker<UnityEngine.GameObject>(new UnityEngine.Object(), false, " ", 2);
                        }
                        else
                        {
                            CreateDoor();
                        }
                        //CreateDoor();
                        break;
                    case 1:
                        Debug.LogWarning("Select a model with the same name as the model imported.");
                        if (!selectedObject)
                        {
                            openFromMissing = true;
                            EditorGUIUtility.ShowObjectPicker<UnityEngine.GameObject>(new UnityEngine.Object(), false, " ", 2);
                        }
                        else
                        {
                            CreateRoom();
                        }
                        break;
                    case 2:
                        Debug.LogWarning("Select a model with the same name as the model imported.");
                        if (!selectedObject)
                        {
                            openFromMissing = true;
                            EditorGUIUtility.ShowObjectPicker<UnityEngine.GameObject>(new UnityEngine.Object(), false, " ", 2);
                        }
                        else
                        {
                            CreateProp();
                        }
                        break;
                    case 3:
                        CreateLight();
                        break;
                }
                //CreateDoor();
            }
            GUILayout.FlexibleSpace();
        }
        GUILayout.EndHorizontal();
        selectedObject = EditorGUILayout.ObjectField(selectedObject, typeof(GameObject), false);
        string commandName = Event.current.commandName;
        //if (commandName == "ObjectSelectorUpdated")
        //{
        //    currentObject = EditorGUIUtility.GetObjectPickerObject();
        //}
        if (commandName == "ObjectSelectorClosed")
        {
            if (openFromMissing)
            {
                selectedObject = EditorGUIUtility.GetObjectPickerObject();
                switch (index)
                {
                    case 0:
                        CreateDoor();
                        break;
                    case 1:
                        CreateRoom();
                        break;
                    case 2:
                        CreateProp();
                        break;
                    case 3:
                        CreateLight();
                        break;
                }
                openFromMissing = false;
            }
        }
        //if(index == 3)
        //{
        //    // EditorGUILayout.PrefixLabel("light Color: ");
        //    lightTypeIndex = EditorGUILayout.Popup("Light Type: ",lightTypeIndex, lightTypes);
        //    lightColor = EditorGUILayout.ColorField("Light Color:",lightColor);
        //}
        //EditorGUIUtility.ShowObjectPicker<UnityEngine.GameObject>(new UnityEngine.Object(), false, " ", 2);
    }
    void CreateDoor()
    {
        Debug.LogWarning("Dont forget to drag to prefab folder");
        Debug.LogWarning("Add rooms this door can spawn!");
        var newobj = new GameObject(nameField);
        var door = newobj.AddComponent<Door>();
        var model = Instantiate(selectedObject) as GameObject;
        model.transform.SetParent(newobj.transform);
        model.transform.position = Vector3.zero;
        model.GetComponent<Animator>().runtimeAnimatorController = Resources.Load("Door Controller") as RuntimeAnimatorController;
        door.Tags = new string[] { nameField };
        if (!SceneView.lastActiveSceneView.camera)
        {
            Debug.LogError("Must have the scene view open!");
            return;
        }
        Camera sceneCam = SceneView.lastActiveSceneView.camera;
        if (sceneCam)
        {
            Vector3 spawnPos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
            door.transform.position = spawnPos;
        }
        for (var i = 0; i < 2; i++)
        {
            var newConnector = new GameObject("ModuleConnector");
            var con = newConnector.AddComponent<ModuleConnector>();
            con.Tags = new string[0];
            con.transform.SetParent(door.transform);
            if (i == 0)
            {
                con.transform.localPosition = new Vector3(0f, 0f, 0.042f);
            }
            else
            {
                con.transform.localPosition = new Vector3(0f, 0f, -0.042f);
                Vector3 newRotation = new Vector3(0f, 180f, 0f);
                con.transform.eulerAngles = newRotation;
            }
        }
        Undo.RegisterCreatedObjectUndo(newobj, "Created Door");
        Selection.activeGameObject = newobj;
    }
    void CreateRoom()
    {
        Debug.LogWarning("Dont forget to drag to prefab folder");
        Debug.LogWarning("Add doors this room can spawn!");
        var newobj = new GameObject(nameField);
         newobj.AddComponent<BoxCollider>().isTrigger = true;
        var room = newobj.AddComponent<Room>();
        var model = Instantiate(selectedObject) as GameObject;
        model.transform.SetParent(newobj.transform);
        model.transform.position = Vector3.zero;
        room.Tags = new string[] { "New Room" };
        if (!SceneView.lastActiveSceneView.camera)
        {
            Debug.LogError("Must have the scene view open!");
            return;
        }
        Camera sceneCam = SceneView.lastActiveSceneView.camera;
        if (sceneCam)
        {
            Vector3 spawnPos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
            room.transform.position = spawnPos;
        }
        Undo.RegisterCreatedObjectUndo(newobj, "Created Room");
        Selection.activeGameObject = newobj;
    }
    void CreateProp()
    {
        Debug.LogWarning("Dont forget to drag to prefab folder");
        var newobj = new GameObject(nameField);
        var prop = newobj.AddComponent<Prop>();
        var model = Instantiate(selectedObject) as GameObject;
        model.transform.SetParent(newobj.transform);
        model.transform.position = Vector3.zero;
        prop.Tags = new string[] { nameField };
        if (!SceneView.lastActiveSceneView.camera)
        {
            Debug.LogError("Must have the scene view open!");
            return;
        }
        Camera sceneCam = SceneView.lastActiveSceneView.camera;
        if (sceneCam)
        {
            Vector3 spawnPos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
            prop.transform.position = spawnPos;
        }
        var newConnector = new GameObject("ModuleConnector");
        var con = newConnector.AddComponent<ModuleConnector>();
        con.Tags = new string[0];
        con.transform.SetParent(prop.transform);
        con.transform.localPosition = new Vector3(0f, 0f, 0f);
        Vector3 newRotation = new Vector3(0f, 0f, 0f);
        con.transform.eulerAngles = newRotation;
        con.IsDefault = true;
        Undo.RegisterCreatedObjectUndo(newobj, "Created Prop");
        Selection.activeGameObject = newobj;
    }
    void CreateLight()
    {
        Debug.LogWarning("Dont forget to drag to prefab folder");
        var newobj = new GameObject(nameField);
        var prop = newobj.AddComponent<Prop>();
        var light = new GameObject("Light");
        light.transform.SetParent(newobj.transform);
        var ltObj = light.AddComponent<Light>();
        ltObj.color = lightColor;
        prop.Tags = new string[] { nameField };
        if (!SceneView.lastActiveSceneView.camera)
        {
            Debug.LogError("Must have the scene view open!");
            return;
        }
        Camera sceneCam = SceneView.lastActiveSceneView.camera;
        if (sceneCam)
        {
            Vector3 spawnPos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10f));
            prop.transform.position = spawnPos;
        }
        var newConnector = new GameObject("ModuleConnector");
        var con = newConnector.AddComponent<ModuleConnector>();
        con.Tags = new string[0];
        con.transform.SetParent(prop.transform);
        con.transform.localPosition = new Vector3(0f, 0f, 0f);
        Vector3 newRotation = new Vector3(0f, 0f, 0f);
        con.transform.eulerAngles = newRotation;
        con.IsDefault = true;
        Undo.RegisterCreatedObjectUndo(newobj, "Created Prop");
        Selection.activeGameObject = newobj;
    }
}
