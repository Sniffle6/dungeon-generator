﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ModuleConnector))]
[CanEditMultipleObjects]
public class ModuleConnectorEditor : Editor
{
    Color lightColor = Color.white;
    string TestOrDelete = "";
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ModuleConnector myScript = (ModuleConnector)target;
        if (!myScript.testingProp)
        {
            TestOrDelete = "Test";
        }else
        {
            TestOrDelete = "Delete";
        }
        if (myScript.Tags.Length > 0)
        {
            if (myScript.Tags[0].Contains("Door")) {
                if (GUILayout.Button(TestOrDelete+" Door"))
                {
                    if (myScript.testingProp)
                    {
                        DestroyImmediate(myScript.testProp);
                        myScript.testProp = null;
                        myScript.testingProp = false;
                    }
                    else
                    {
                        TestProp(myScript, "Door");
                    }
                }
            }else if (myScript.Tags[0].Contains("TubeLight"))
            {
                if (GUILayout.Button(TestOrDelete + " Light"))
                {
                    if (myScript.testingProp)
                    {
                        DestroyImmediate(myScript.testProp);
                        myScript.testProp = null;
                        myScript.testingProp = false;
                    }
                    else
                    {
                        TestProp(myScript, "TubeLight");
                    }
                }
            }else
            {
                if (GUILayout.Button(TestOrDelete + " Prop"))
                {
                    if (myScript.testingProp)
                    {
                        DestroyImmediate(myScript.testProp);
                        myScript.testProp = null;
                        myScript.testingProp = false;
                    }
                    else
                    {
                        TestProp(myScript, "Sodacan");
                    }
                }
            }
        }
        if (myScript.Tags.Length > 0 && myScript.Tags[0] == "TubeLight")
        {
            myScript.randomLightcolor = EditorGUILayout.Toggle("Random Color?", myScript.randomLightcolor);
            myScript.m_Intensity = EditorGUILayout.FloatField("Light Intensity", myScript.m_Intensity);
            if (!myScript.randomLightcolor)
                myScript.m_Color = EditorGUILayout.ColorField("Light Color:", myScript.m_Color);
            myScript.m_Range = EditorGUILayout.FloatField("Light Range", myScript.m_Range);
            myScript.m_Radius = EditorGUILayout.FloatField("Light Radius", myScript.m_Radius);
            myScript.m_Length = EditorGUILayout.FloatField("Light Length", myScript.m_Length);
        }
    }
    void TestProp(ModuleConnector script, string prop)
    {
        var prefab = Resources.Load("Modules/"+ prop) as GameObject;
        script.testProp = Instantiate(prefab);
        script.testProp.transform.SetParent(script.transform);
        script.testProp.transform.localPosition = Vector3.zero;
       
        Vector3 newRotation = new Vector3(script.transform.eulerAngles.x, script.transform.eulerAngles.y, script.transform.eulerAngles.z);
        script.testProp.transform.eulerAngles = newRotation;
        script.testingProp = true;
        Undo.RegisterCreatedObjectUndo(script.testProp, "Created Test "+prop);
        Undo.RegisterCompleteObjectUndo(script, "Changed ModuleConnector");
    }
}
