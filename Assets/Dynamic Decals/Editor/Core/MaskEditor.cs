﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(ProjectionMask))]
public class ProjectionMaskEditor : Editor
{
    SerializedProperty layers;

    private void OnEnable()
    {
        layers = serializedObject.FindProperty("layers");

        //Register to Undo callback
        Undo.undoRedoPerformed += OnUndoRedo;
    }
    private void OnDisable()
    {
        //Deregister to Undo callback
        Undo.undoRedoPerformed -= OnUndoRedo;
    }
    private void OnUndoRedo()
    {
        ((ProjectionMask)target).Update();
    }

    public override void OnInspectorGUI()
    {
        //Update Object
        serializedObject.Update();

        //Grab our target
        ProjectionMask mask = (ProjectionMask)target;

        EditorGUILayout.Space();

        if (!mask.Enabled)
        {
            EditorGUILayout.HelpBox("No active renderer found. Masks will only be drawn when an active renderer with a valid mesh is attached.", MessageType.Warning);
            EditorGUILayout.Space();
        }

        EditorGUILayout.LabelField(new GUIContent("Layers", "The mask layers avaliable to draw to"), EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        for (int i = 0; i < layers.arraySize; i++)
        {
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(layers.GetArrayElementAtIndex(i), new GUIContent(DynamicDecals.Settings.layerNames[i], "Draw to this mask layer?"));
            if (EditorGUI.EndChangeCheck()) mask.Update();
        }
        
        EditorGUI.indentLevel--;

        EditorGUILayout.Space();

        //Apply Modified Properties
        serializedObject.ApplyModifiedProperties();
    }
}
