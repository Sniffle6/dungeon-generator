﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(RayPrinter))]
public class RayPrinterEditor : PrinterEditor
{
    SerializedProperty layers;

    public override void OnEnable()
    {
        base.OnEnable();

        layers = serializedObject.FindProperty("layers");
    }

    public override void OnInspectorGUI()
    {
        //Update object
        serializedObject.Update();

        PrintGUI();
        PoolGUI();
        ParentGUI();
        OverlapGUI();
        FadeGUI();
        FrequencyGUI();

        LayerGUI();

        //Apply modified properties
        serializedObject.ApplyModifiedProperties();
    }

    private void LayerGUI()
    {
        EditorGUILayout.LabelField(new GUIContent("Layers", "Which layers to cast against"));
        EditorGUI.indentLevel++;
        EditorGUILayout.PropertyField(layers, new GUIContent("", "Which layers to cast against"));
        EditorGUI.indentLevel--;
        EditorGUILayout.Space();
    }
}
