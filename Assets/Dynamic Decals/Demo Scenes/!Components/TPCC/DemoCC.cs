﻿using UnityEngine;
using System.Collections;

//Character Controller
public class DemoCC : MonoBehaviour
{
    #region Inspector Variables
    [Header("Look")]
    public float lookSensitivity = 0.3f;
    public float lookSpeed = 0.2f;
    public AnimationCurve lookCurve;

    [Header("Rotation")]
    public float rotationSensitivity = 1.0f;

    [Header("Movement")]
    public float movementSpeed = 0.1f;

    [Header("Angle")]
    public float Angle = 44f;
    public float AngleSmooth = 0.3f;

    [Header("Zoom")]
    public float defaultZoom = 14.0f;
    public float minZoom = 6.0f;
    public float maxZoom = 40.0f;
    public float zoomSpeed = 12.0f;

    [Header("Field of View")]
    public float fieldOfView = 110;
    public float minFOV = 80;
    public float maxFOV = 110;
    #endregion
    #region Calculation Variables
    //References
    public Camera controlledCamera;
    public DemoPC player;

    //Offset
    private Vector2 screenOffset;

    //Angle
    private float cameraAngle;
    private float angleVelocity;

    //Zoom
    private float zoom;
    private Vector3 cameraOffset;

    //Movement
    private Vector3 basePos;
    private Vector3 cameraVelocity;

    //LookOffset
    private Vector3 offset;
    private Vector3 offsetVelocity;

    //Rotation
    private float initialMouseX;
    private float currentMousex;
    private float initialRotOffset;
    private float currentRotOffset;

    //Input
    private bool rotationInput;
    #endregion

    #region Generic Methods
    void Awake()
    {
        controlledCamera = GetComponentInChildren<Camera>();
    }
    void Start()
    {
        //Set default zoom
        zoom = defaultZoom;

        //Set default angle
        cameraAngle = Angle;
    }
    #endregion

    #region Input
    public void Update()
    {
        //Screen Offset
        Vector2 screenCentre = new Vector2(Screen.width / 2, Screen.height / 2);
        screenOffset.x = (Input.mousePosition.x - screenCentre.x) / screenCentre.x;
        screenOffset.y = (Input.mousePosition.y - screenCentre.y) / screenCentre.y;

        //Zoom Input
        zoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
        zoom = Mathf.Clamp(zoom, minZoom, maxZoom);

        //RotationInput
        if (Input.GetMouseButtonDown(2))
        {
            initialMouseX = Input.mousePosition.x;
            initialRotOffset = currentRotOffset;
        }
        if (Input.GetMouseButton(2))
        {
            rotationInput = true;
        }
        else
        {
            rotationInput = false;
        }
    }
    #endregion
    #region Logic
    void LateUpdate()
    {
        if (player != null)
        {
            //Base position
            basePos = Vector3.SmoothDamp(basePos, player.transform.position, ref cameraVelocity, movementSpeed);

            //Offset position
            screenOffset = screenOffset.normalized * lookCurve.Evaluate(screenOffset.magnitude) * lookSensitivity;
                
            //Convert screen offset into a position offset
            Vector3 positionOffset = (Forward * screenOffset.y) + (Right * screenOffset.x);
                
            //Smooth offset
            offset = Vector3.SmoothDamp(offset, positionOffset, ref offsetVelocity, lookSpeed);

            //Final Position
            transform.position = basePos + (offset * zoom);

            //Rotation
            if (rotationInput)
            {
                currentMousex = Input.mousePosition.x;
                currentRotOffset = initialRotOffset - ((currentMousex - initialMouseX) * rotationSensitivity);
            }

            //Camera Angle
            cameraAngle = Mathf.SmoothDampAngle(cameraAngle, Angle, ref angleVelocity, AngleSmooth);

            //Generate a CameraOffset from our CurrentZoom and Angle
            float FielfOfViewAdjust = Mathf.Pow(fieldOfView / maxFOV, -1);

            cameraOffset = Vector3.zero;
            cameraOffset.y = zoom * FielfOfViewAdjust * (1.41f * Mathf.Sin(Mathf.Deg2Rad * cameraAngle));
            cameraOffset.z = -zoom * FielfOfViewAdjust * (1.41f * Mathf.Cos(Mathf.Deg2Rad * cameraAngle));

            //Camera Position
            controlledCamera.transform.position = RotateAroundPoint(transform.position + cameraOffset, transform.position, Quaternion.Euler(new Vector3(0, currentRotOffset, 0)));

            //Camera Rotation
            controlledCamera.transform.rotation = Quaternion.LookRotation(transform.position - controlledCamera.transform.position);
        }
    }
    #endregion

    #region External Access
    //FOV
    public float FieldOfView
    {
        set
        {
            if (controlledCamera == null)
            {
                controlledCamera = GetComponentInChildren<Camera>();
            }
            controlledCamera.fieldOfView = HorizontalToVerticalFOV(Mathf.Clamp(fieldOfView, minFOV, maxFOV), controlledCamera.aspect);
        }
    }
        
    //Camera Directions
    public Vector3 Forward
    {
        get
        {
            return (Vector3.Cross(controlledCamera.transform.right, Vector3.up)).normalized;
        }
    }
    public Vector3 Right
    {
        get { return (Vector3.Cross(-Forward, Vector3.up)).normalized; }
    }

    //Camera Rotations
    public Quaternion Rotation
    {
        get { return Quaternion.LookRotation(transform.position - controlledCamera.transform.position); }
    }
    public Quaternion InverseRotation
    {
        get { return Quaternion.LookRotation(controlledCamera.transform.position - transform.position); }
    }
    #endregion

    #region Utility
    private static float HorizontalToVerticalFOV(float horizontalFOV, float aspect)
    {
        return Mathf.Rad2Deg * 2 * Mathf.Atan(Mathf.Tan((horizontalFOV * Mathf.Deg2Rad) / 2f) / aspect);
    }
    private Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion Angle)
    {
        return Angle * (point - pivot) + pivot;
    }
    #endregion
}