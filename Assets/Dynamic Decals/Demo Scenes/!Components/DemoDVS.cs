﻿using UnityEngine;
using System.Collections;

//Disable V-Sync
public class DemoDVS : MonoBehaviour {

	void Start ()
    {
        QualitySettings.vSyncCount = 0;
	}
}