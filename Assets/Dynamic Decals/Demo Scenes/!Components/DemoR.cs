﻿using UnityEngine;
using System.Collections;

//Rotater
public class DemoR : MonoBehaviour {

    public float rotationSpeed = 10;
	
	void Update ()
    {
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
	}
}
