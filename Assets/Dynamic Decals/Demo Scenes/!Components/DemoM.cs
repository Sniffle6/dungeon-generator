﻿using UnityEngine;
using System.Collections;

//Mover
public class DemoM : MonoBehaviour {

    public MoveMethod method = MoveMethod.Linear;
    public float speed = 0.25f;
    public float breakDistance = 0.5f;
    public Vector3[] targets;

    private int target;
    private Vector3 velocity;
	
	void Update ()
    {
        //If we have reached our target, grab the next
        if (Vector3.Distance(transform.localPosition, targets[target]) < breakDistance)
        {
            if (target < targets.Length - 1) target++;
            else target = 0;
        }

        switch (method)
        {
            case MoveMethod.Linear:
                //Move towards our target
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, targets[target], speed * Time.deltaTime);
                break;
            case MoveMethod.Exponential:
                //SmoothDamp towards our target
                transform.localPosition = Vector3.SmoothDamp(transform.localPosition, targets[target], ref velocity, speed);
                break;
        }
	}

    public enum MoveMethod { Linear, Exponential };
}
