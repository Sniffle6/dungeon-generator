﻿using UnityEngine;
using System.Collections;

//Weapon Controller
public class DemoWC : MonoBehaviour {

    public Camera cameraController;
    public DemoFPC controller;
    public Transform parent;

    [Header("Positioning")]
    public Vector3 weaponOffset = new Vector3(0.5f, -0.24f, 0.5f);
    public Vector3 weaponRotOffset = new Vector3(0, 0, 0);

    [Header("Aiming")]
    public LayerMask layers;
    public float aimSmooth = 60;
    public float defaultAimDistance = 40;

    //Aiming
    private Vector3 targetPosition;

    //Input
    protected bool primary;
    protected bool secondary;
    protected bool alternate;    

    //Projectile fire
    protected float timeToFire;

    void OnEnable()
    {
        if (cameraController == null) cameraController = Camera.main;
        if (transform.parent != cameraController) transform.SetParent(cameraController.transform);

        //Update weapon position
        transform.position = cameraController.transform.TransformPoint(weaponOffset);
    }

    void Update()
    {
        //Check for Input
        primary = (Input.GetMouseButton(0)) ? true : false;
        secondary = (!Input.GetMouseButton(0) && Input.GetMouseButton(1)) ? true : false;
        alternate = (!Input.GetMouseButton(0) && !Input.GetMouseButton(1) && Input.GetMouseButton(2)) ? true : false;
    }

    public virtual void UpdateWeapon()
    {
        Aim();

        //Increment time since fired
        timeToFire = Mathf.Clamp(timeToFire - Time.fixedDeltaTime, 0, Mathf.Infinity);
    }
    private void Aim()
    {
        //Calculate target position
        if (Application.isPlaying)
        {
            RaycastHit hit;
            if (Physics.Raycast(cameraController.transform.position, cameraController.transform.forward, out hit, Mathf.Infinity, layers.value))
            {
                targetPosition = hit.point;
            }
            else
            {
                targetPosition = cameraController.transform.position + cameraController.transform.forward * defaultAimDistance;
            }

            //Aim
            Quaternion GoalRotation = Quaternion.LookRotation((targetPosition - transform.position).normalized, Vector3.up) * Quaternion.Euler(weaponRotOffset);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, GoalRotation, aimSmooth * Time.deltaTime);
        }
        else
        {
            //Aim
            targetPosition = cameraController.transform.position + cameraController.transform.forward * defaultAimDistance;
            transform.rotation = Quaternion.LookRotation((targetPosition - transform.position).normalized, Vector3.up) * Quaternion.Euler(weaponRotOffset);
        }
    }
}