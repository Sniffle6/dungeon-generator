﻿using UnityEngine;
using System.Collections;

//First Person Character Controller
[ExecuteInEditMode]
public class DemoFPC : MonoBehaviour
{
    [Header("Look")]
    public float lookSensitivity = 3f;

    [Header("Move")]
    public float moveSpeed = 5;

    [Header("Jump")]
    public float jumpHeight = 8;

    [Header("Camera")]
    public Camera cameraControlled;
    public float cameraSmooth = 0.2f;
    public Vector3 cameraOffset = new Vector3 (0,0.6f,0);

    [Header("Weapon")]
    public DemoWC weapon;

    //Components
    private Rigidbody attachedRigidbody;
    private CapsuleCollider capsuleCollider;

    //Looking
    private Vector3 cameraRotation;
    private Vector2 lookDelta;

    //Recoil
    private float recoil;
    private float recoilDuration;
    private float recoilVelocity;

    //Moving
    private Vector3 moveDelta;

    //Jumping
    private bool jumpInput;
    private bool Grounded
    {
        get { return Physics.Raycast(transform.position, -Vector3.up, capsuleCollider.bounds.extents.y * 1.2f); }
    }

    //Camera Smooth
    private Vector3 cameraVelocity;

    void Awake()
    {
        //Grab our components
        attachedRigidbody = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();

        if (Application.isPlaying)
        {
            //Lock Cursor
            Cursor.lockState = CursorLockMode.Confined;

            //Hide Cursor
            Cursor.visible = false;
        }
    }

    void OnEnable()
    {
        if (cameraControlled == null) cameraControlled = Camera.main;
        cameraRotation = cameraControlled.transform.rotation.eulerAngles;
    }
	
    void Update()
    {
        //Look Input
        lookDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        //Move Input
        moveDelta = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

        //Jump Input
        if (Input.GetKey(KeyCode.Space)) jumpInput = true;
        else jumpInput = false;
    }

    void FixedUpdate()
    {
        //Get velocity
        Vector3 velocity = attachedRigidbody.velocity;

        //Update character position
        Vector3 goalPosition = transform.TransformPoint(moveDelta * moveSpeed) - transform.position;
        velocity.x = goalPosition.x;
        velocity.z = goalPosition.z;

        //Update character rotation
        Vector3 characterRotation = transform.rotation.eulerAngles;
        characterRotation.y += lookDelta.x * lookSensitivity;
        transform.rotation = Quaternion.Euler(characterRotation);

        //Jumping
        if (jumpInput && Grounded)
        {
            velocity.y = jumpHeight;
        }

        //Set velocity
        attachedRigidbody.velocity = velocity;

        //Update camera rotation
        cameraRotation.x -= lookDelta.y * lookSensitivity;
        cameraRotation.y += lookDelta.x * lookSensitivity;
        cameraRotation.z = 0;

        //Clamp looking too high/low
        if (cameraRotation.x < 200) cameraRotation.x = Mathf.Clamp(cameraRotation.x, -80, 80);
        else cameraRotation.x = Mathf.Clamp(cameraRotation.x, 280, 360);


        //Update recoil
        recoil = Mathf.SmoothDamp(recoil, 0, ref recoilVelocity, recoilDuration);
        Vector3 RecoiledRotation = cameraRotation;
        RecoiledRotation.x -= recoil;

        cameraControlled.transform.rotation = Quaternion.Euler(RecoiledRotation);

        //Update camera position
        cameraControlled.transform.position = Vector3.SmoothDamp(cameraControlled.transform.position, transform.TransformPoint(cameraOffset), ref cameraVelocity, cameraSmooth);

        //Update weapon - Called here instead of within its own FixedUpdate because we need to guarentee it's not updated until after the camera position has been
        if(weapon != null) weapon.UpdateWeapon();
    }

    public void ApplyRecoil(float RecoilStrength, float RecoilDuration)
    {
        recoilDuration = RecoilDuration;
        recoilVelocity += RecoilStrength;
    }
}