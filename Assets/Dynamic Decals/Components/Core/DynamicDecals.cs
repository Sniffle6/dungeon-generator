﻿using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

/*! \mainpage Introduction
 * Welcome to the Dynamic Decals script documentation. Here you will find everything you need to interact with the front end of the Dynamic Decals system.
 * The Documentation covers all projections available within the system, as well as all printers, projectors & the in-built pooling system.
 *
 * If you want to extend or tinker with the system, the backend code is completely open and thoroughly commented & so are all the shaders, so feel free. 
 * However please note that as it's not necessary for 99% of users, is subject to change and would clutter the documentation, the backend of the system & shaders have been left out of the documentation.
 *
 * If you get stuck hit me up at Support@LlockhamIndustustries.com. We've all been stuck at one stage or another, I wouldn't wish it on anyone. Always more than happy to help. 
 * If you use the system to make something cool, send my screenshots, I want to see.
 *
 * Happy scripting, Dan.
 */

/**
* The core class of the system, responsible for the majority of the systems functionality. 
* For scripting purposes, it's almost entirely a black box, you should rarely need to access or modify anything within it.
* It's well stuctured and commented all the same though, so if your interested, open it up and have a look around.
*/
public class DynamicDecals
{
    //Static Initializer
    private static DynamicDecals System
    {
        get
        {
            if (system == null)
                system = new DynamicDecals();
            return system;
        }
    }
    private static DynamicDecals system;

    //Deconstructor
    ~DynamicDecals()
    {
        if (system.initialized) Debug.LogWarning("System shutting down via Deconstructor. Forcing Uninitialization");

        //Occasionally the class is cleaned before all projections have a chance to De-register
        //When this occurs the class will not cleanup culling groups correctly. Thusly we uninitialize in the class destructor as well
        Uninitialize(true);
    }

    #region System Settings
    public static DynamicDecalSettings Settings
    {
        get
        {
            if (System.settings == null) System.settings = Resources.Load<DynamicDecalSettings>("Settings");

            #if UNITY_EDITOR
            //If in editor, settings may have been changed recently, so reload each time we access them
            Resources.Load<DynamicDecalSettings>("Settings");
            #endif

            if (System.settings == null) System.settings = ScriptableObject.CreateInstance<DynamicDecalSettings>();

            return System.settings;
        }
    }
    private DynamicDecalSettings settings;
    #endregion
    #region Rendering Settings
    //Rendering Path
    public static RenderingPath RenderingPath
    {
        get;
        private set;
    }

    //Methods
    private static void CheckRenderingPath()
    {
        Camera target = null;
        if (Camera.main != null) target = Camera.main;
        else if (Camera.current != null) target = Camera.current;

        if (target != null)
        {
            //Grab our new path
            if (target.actualRenderingPath == RenderingPath.Forward || target.actualRenderingPath == RenderingPath.DeferredShading)
            {
                //Set the new rendering path
                RenderingPath = target.actualRenderingPath;

                //Make sure all of our valid cameras are using this path
                for (int i = 0; i < cameraData.Count; i++)
                {
                    Camera camera = cameraData.ElementAt(i).Key;
                    CameraData data = cameraData.ElementAt(i).Value;

                    if (camera != null && data.path != RenderingPath)
                    {
                        UpdateRenderingPath(camera, data);
                    }
                }
            }
            else Debug.LogWarning("Current Rendering Path not supported! Please use either Forward or Deferred");
        }
    }
    private static void UpdateRenderingPath(Camera Camera, CameraData Data)
    {
        if (Data.path != RenderingPath)
        {
            //Remove ourself from the old path
            switch (Data.path)
            {
                case RenderingPath.Forward:
                    Camera.RemoveCommandBuffer(ForwardMaskEvent, Data.maskBuffer);
                    break;
                case RenderingPath.DeferredShading:
                    Camera.RemoveCommandBuffer(DeferredMaskEvent, Data.maskBuffer);
                    Camera.RemoveCommandBuffer(DeferredProjectionEvent, Data.projectionBuffer);
                    break;
            }
            //Update our path
            Data.path = RenderingPath;

            //Add ourself to the new rendering path & set out depth texture mode
            switch (Data.path)
            {
                case RenderingPath.Forward:
                    SetForward(Camera, Data);
                    break;

                case RenderingPath.DeferredShading:
                    SetDeferred(Camera, Data);
                    break;
            }
        }
    }

    private static void SetForward(Camera Camera, CameraData CameraData)
    {
        //Add mask command buffer
        Camera.AddCommandBuffer(ForwardMaskEvent, CameraData.maskBuffer);

        //Set our depth texture mode
        SetDepthTextureMode(Camera, CameraData);
    }
    private static void SetDeferred(Camera Camera, CameraData CameraData)
    {
        //Add mask & projection command buffers
        Camera.AddCommandBuffer(DeferredMaskEvent, CameraData.maskBuffer);
        Camera.AddCommandBuffer(DeferredProjectionEvent, CameraData.projectionBuffer);

        //Restore our depth texture mode
        RestoreDepthTextureMode(Camera, CameraData);
    }

    private static void SetDepthTextureMode(Camera Camera, CameraData CameraData)
    {
        if (Camera.depthTextureMode != DepthTextureMode.DepthNormals)
        {
            //Cache the original depth texture mode
            CameraData.depthTextureMode = Camera.depthTextureMode;
            //Set the depth texture mode to DepthNormal
            Camera.depthTextureMode = DepthTextureMode.DepthNormals;
        }
    }
    private static void RestoreDepthTextureMode(Camera Camera, CameraData CameraData)
    {
        //Restore the depth texture mode to the cached
        if (CameraData.depthTextureMode.HasValue)
        {
            Camera.depthTextureMode = CameraData.depthTextureMode.Value;
        }
    }

    public static void RestoreDepthTextureModes()
    {
        //Iterate over every camera and restore it to it's original depth texture mode
        for (int i = 0; i < cameraData.Count; i++)
        {
            Camera camera = cameraData.ElementAt(i).Key;
            if (camera != null) RestoreDepthTextureMode(camera, cameraData.ElementAt(i).Value);
        }
    }

    //Global Settings
    private static CameraEvent ForwardMaskEvent
    {
        get { return CameraEvent.AfterDepthNormalsTexture; }
    }

    private static CameraEvent DeferredMaskEvent
    {
        get { return CameraEvent.BeforeReflections; }
    }
    private static CameraEvent DeferredProjectionEvent
    {
        get { return CameraEvent.BeforeReflections; }
    }
    #endregion
    #region SceneView Settings
    private static void LockClippingPlanes()
    {
    #if UNITY_EDITOR
        float clipSize = 0.1f;

        foreach (SceneView sceneView in SceneView.sceneViews)
        {
            //Only limit clipping planes in 3D mode
            if (!sceneView.in2DMode)
            {
                //Only lock when in deferred or forward rendering
                if (RenderingPath != RenderingPath.Forward && RenderingPath != RenderingPath.DeferredShading) return;

                //Only lock if the settings allow
                if (RenderingPath == RenderingPath.Forward && !Settings.lockForwardDepth) return;
                if (RenderingPath == RenderingPath.DeferredShading && !Settings.lockDeferredDepth) return;

                //Record the position & rotation of the camera
                Vector3 position = sceneView.pivot;
                Quaternion rotation = sceneView.camera.transform.rotation;

                //Record our selection 
                GameObject[] selection = Selection.gameObjects;

                //Create an empty object to focus on
                GameObject focus = GameObject.CreatePrimitive(PrimitiveType.Cube);
                focus.transform.localScale = new Vector3(clipSize, clipSize, clipSize);

                //Select the focus object
                Selection.activeGameObject = focus;

                //Focus object - Setting the clipping planes accordingly
                sceneView.FrameSelected();

                //Move camera back to original position
                sceneView.LookAt(position, rotation);
                sceneView.pivot = position;

                //Make sure were in perspective
                sceneView.orthographic = false;

                //Change selection back to original
                Selection.objects = selection;

                //Destroy focus object
                GameObject.DestroyImmediate(focus, true);
            }
        }
    #endif
    }
    #endregion
    #region Warnings
    private bool sceneFocus = false;
    private bool cameraClipping = false;
    #endregion

    #region Material Data
    public static Material Mat_ForwardBlit
    {
        get
        {
            if (System.forwardBlit == null) System.forwardBlit = new Material(Shader.Find("Decal/ForwardBlit"));
            return System.forwardBlit;
        }
    }
    public static Material Mat_DeferredBlit
    {
        get
        {
            if (System.deferredBlit == null) System.deferredBlit = new Material(Shader.Find("Decal/DeferredBlit"));
            return System.deferredBlit;
        }
    }

    public static Material Mat_Mask
    {
        get
        {
            if (System.mask == null) System.mask = new Material(Shader.Find("Decal/Mask"));
            return System.mask;
        }
    }

    public static Material Mat_Decal_Metallic
    {
        get
        {
            if (System.metallic == null)
            {
                System.metallic = new Material(Shader.Find("Decal/Metallic"));
                System.metallic.DisableKeyword("_AlphaTest");
                System.metallic.EnableKeyword("_Blend");
            }
            return System.metallic;
        }
    }
    public static Material Mat_Decal_MetallicCutout
    {
        get
        {
            if (System.metallicCutout == null)
            {
                System.metallicCutout = new Material(Shader.Find("Decal/Metallic"));
                System.metallicCutout.EnableKeyword("_AlphaTest");
                System.metallicCutout.DisableKeyword("_Blend");
            }
            return System.metallicCutout;
        }
    }
    public static Material Mat_Decal_Specular
    {
        get
        {
            if (System.specular == null)
            {
                System.specular = new Material(Shader.Find("Decal/Specular"));
                System.specular.DisableKeyword("_AlphaTest");
                System.specular.EnableKeyword("_Blend");
            }
            return System.specular;
        }
    }
    public static Material Mat_Decal_SpecularCutout
    {
        get
        {
            if (System.specularCutout == null)
            {
                System.specularCutout = new Material(Shader.Find("Decal/Specular"));
                System.specularCutout.EnableKeyword("_AlphaTest");
                System.specularCutout.DisableKeyword("_Blend");
            }
            return System.specularCutout;
        }
    }
    public static Material Mat_Decal_Unlit
    {
        get
        {
            if (System.unlit == null)
            {
                System.unlit = new Material(Shader.Find("Decal/Unlit"));
                System.unlit.DisableKeyword("_AlphaTest");
                System.unlit.EnableKeyword("_Blend");
            }
            return System.unlit;
        }
    }
    public static Material Mat_Decal_UnlitCutout
    {
        get
        {
            if (System.unlitCutout == null)
            {
                System.unlitCutout = new Material(Shader.Find("Decal/Unlit"));
                System.unlitCutout.EnableKeyword("_AlphaTest");
                System.unlitCutout.DisableKeyword("_Blend");
            }
            return System.unlitCutout;
        }
    }

    public static Material Mat_Decal_Roughness
    {
        get
        {
            if (System.roughness == null)
            {
                System.roughness = new Material(Shader.Find("Decal/Roughness"));
                System.roughness.DisableKeyword("_AlphaTest");
                System.roughness.EnableKeyword("_Blend");
            }
            return System.roughness;
        }
    }
    public static Material Mat_Decal_RoughnessCutout
    {
        get
        {
            if (System.roughnessCutout == null)
            {
                System.roughnessCutout = new Material(Shader.Find("Decal/Roughness"));
                System.roughnessCutout.EnableKeyword("_AlphaTest");
                System.roughnessCutout.DisableKeyword("_Blend");
            }
            return System.roughnessCutout;
        }
    }

    public static Material Mat_Decal_Normal
    {
        get
        {
            if (System.normal == null)
            {
                System.normal = new Material(Shader.Find("Decal/Normal"));
                System.normal.DisableKeyword("_AlphaTest");
                System.normal.EnableKeyword("_Blend");
            }
            return System.normal;
        }
    }
    public static Material Mat_Decal_NormalCutout
    {
        get
        {
            if (System.normalCutout == null)
            {
                System.normalCutout = new Material(Shader.Find("Decal/Normal"));
                System.normalCutout.EnableKeyword("_AlphaTest");
                System.normalCutout.DisableKeyword("_Blend");
            }
            return System.normalCutout;
        }
    }

    public static Material Mat_Pulse
    {
        get
        {
            if (System.pulse == null)
            {
                System.pulse = new Material(Shader.Find("Decal/Pulse"));
                System.pulse.DisableKeyword("_AlphaTest");
                System.pulse.EnableKeyword("_Blend");
            }
            return System.pulse;
        }
    }
    public static Material Mat_PulseCutout
    {
        get
        {
            if (System.pulseCutout == null)
            {
                System.pulseCutout = new Material(Shader.Find("Decal/Pulse"));
                System.pulseCutout.EnableKeyword("_AlphaTest");
                System.pulseCutout.DisableKeyword("_Blend");
            }
            return System.pulseCutout;
        }
    }

    public static Material Mat_Eraser
    {
        get
        {
            if (System.eraser == null)
            {
                System.eraser = new Material(Shader.Find("Decal/Eraser"));
                System.eraser.DisableKeyword("_AlphaTest");
                System.eraser.EnableKeyword("_Blend");
            }
            return System.eraser;
        }
    }
    public static Material Mat_EraserCutout
    {
        get
        {
            if (System.eraserCutout == null)
            {
                System.eraserCutout = new Material(Shader.Find("Decal/Eraser"));
                System.eraserCutout.EnableKeyword("_AlphaTest");
                System.eraserCutout.DisableKeyword("_Blend");
            }
            return System.eraserCutout;
        }
    }
    public static Material Mat_EraserGrab
    {
        get
        {
            if (System.eraserGrab == null)
            {
                System.eraserGrab = new Material(Shader.Find("Decal/EraserGrab"));
            }
            return System.eraserGrab;
        }
    }

    //Backing Fields
    private Material forwardBlit;
    private Material deferredBlit;
    private Material mask;

    private Material metallic;
    private Material metallicCutout;

    private Material specular;
    private Material specularCutout;

    private Material unlit;
    private Material unlitCutout;

    private Material roughness;
    private Material roughnessCutout;

    private Material normal;
    private Material normalCutout;

    private Material pulse;
    private Material pulseCutout;

    private Material eraser;
    private Material eraserCutout;
    private Material eraserGrab;
    #endregion
    #region Mesh Data
    public static Mesh Cube
    {
        get
        {
            if (System.cube == null)
            {
                GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
                System.cube = gameObject.GetComponent<MeshFilter>().sharedMesh;
                GameObject.DestroyImmediate(gameObject);
            }
            return System.cube;
        }
    }
    private Mesh cube;
    #endregion
    #region Projection Data
    //Add or Remove Projections
    public static void AddProjection(Projection Projection)
    {
        //Make sure the system is initialized
        Initialize();

        //Initialize list
        if (System.projections == null) System.projections = new List<Projection>();

        //Add projection
        if (!system.projections.Contains(Projection))
        {
            //If count is 0 add
            if (system.projections.Count == 0)
            {
                system.projections.Add(Projection);
            }
            else
            {
                //Projection are ordered from lowest priority to highest
                for (int i = 0; i < system.projections.Count; i++)
                {
                    //If we are lower priority than i, insert before i
                    if (Projection.Priority < system.projections[i].Priority)
                    {
                        system.projections.Insert(i, Projection);
                        break;
                    }
                    //If we are of equal priority to i, but of lower timeID, insert before i
                    if (Projection.Priority == system.projections[i].Priority && Projection.timeID < system.projections[i].timeID)
                    {
                        system.projections.Insert(i, Projection);
                        break;
                    }
                    //If we aren't lower than anything in the list, just add ourself
                    if (i == system.projections.Count - 1)
                    {
                        system.projections.Add(Projection);
                        break;
                    }
                }
            }

            //Static check
            if (Projection.GetType() == typeof(Eraser)) system.staticCount++;
        }
    }
    public static void RemoveProjection(Projection Projection)
    {
        //Remove projection
        if (system.projections.Remove(Projection))
        {
            //Static check
            if (Projection.GetType() == typeof(Eraser)) system.staticCount = Mathf.Clamp(system.staticCount - 1, 0, 10000000);
        }

        //If there are no projections left, shut the system down
        Uninitialize();
    }

    //Sort Projections
    private bool sort;
    public static void Sort()
    {
        system.sort = true;
    }
    private static void ReorderProjections()
    {
        //We only need to sort in deferred rendering.
        //In forward rendering the order of our projections is meaningless.
        if (System.sort && RenderingPath == RenderingPath.DeferredShading)
        {
            //Sort from lowest priority to highest
            System.projections.Sort((x, y) =>
            {
                if (x.Priority > y.Priority) return 1;
                else if (x.Priority < y.Priority) return -1;
                else if (x.timeID > y.timeID) return 1;
                else if (x.timeID < y.timeID) return -1;
                else return 0;
            });

            //No longer need to sort
            System.sort = false;
        }
    }

    //Update Projections
    private static void UpdateProjections()
    {
        for (int i = 0; i < system.projections.Count; i++)
        {
            system.projections[i].UpdateProjection();
        }
    }

    //Projections
    private List<Projection> projections;

    //Projection Culling Spheres
    private BoundingSphere[] projectionSpheres;

    //Static Requirements
    public static bool StaticPass
    {
        get { return (System.staticCount > 0) ? true : false; }
    }
    private int staticCount;
    #endregion
    #region Masks Data
    //Add / Remove Masks
    public static void AddMask(ProjectionMask Mask)
    {
        //Initialize list
        if (System.masks == null) System.masks = new List<ProjectionMask>();

        //Add mask
        if (!system.masks.Contains(Mask))
        {
            system.masks.Add(Mask);
        }
    }
    public static void RemoveMask(ProjectionMask Mask)
    {
        //Initialize list
        if (System.masks == null) System.masks = new List<ProjectionMask>();

        system.masks.Remove(Mask);
    }

    //Masks
    private List<ProjectionMask> masks;

    //Mask Culling
    private BoundingSphere[] maskSpheres;
    #endregion
    #region Camera Data
    internal static Dictionary<Camera, CameraData> cameraData = new Dictionary<Camera, CameraData>();
    internal class CameraData
    {
        //Current Path
        public RenderingPath path;

        //Buffers
        public CommandBuffer maskBuffer;
        public CommandBuffer projectionBuffer;

        //Culling Groups
        public CullingGroup maskCulling;
        public CullingGroup projectionCulling;

        //Original Depth Texture Mode
        public DepthTextureMode? depthTextureMode = null;

        //Camera type
        public bool sceneCamera;
        public bool previewCamera;

        public CameraData(Camera Camera, RenderingPath Path, CommandBuffer MaskBuffer, CommandBuffer ProjectionBuffer, CullingGroup MaskCulling, CullingGroup ProjectionCulling)
        {
            path = Path;

            maskBuffer = MaskBuffer;
            projectionBuffer = ProjectionBuffer;

            maskCulling = MaskCulling;
            projectionCulling = ProjectionCulling;

            sceneCamera = (Camera.name == "SceneCamera");
            previewCamera = (Camera.name == "Preview Camera");
        }
    }

    internal static CameraData GetData(Camera Camera)
    {
        //Declare our Camera Data
        CameraData data;

        //Check if this is a camera has a camera data
        if (!cameraData.TryGetValue(Camera, out data))
        {
            //Grab our rendering path
            RenderingPath path = Camera.actualRenderingPath;

            //Create & register our buffers
            CommandBuffer maskBuffer = new CommandBuffer();
            maskBuffer.name = "Dynamic Decals - Masking";

            CommandBuffer projectionBuffer = new CommandBuffer();
            projectionBuffer.name = "Dynamic Decals - Projection";

            //No culling groups found for this camera, so create them & register
            CullingGroup maskCulling = new CullingGroup();
            CullingGroup projectionCulling = new CullingGroup();

            //Register culling group to camera
            maskCulling.targetCamera = Camera;
            projectionCulling.targetCamera = Camera;

            //Generate data
            data = new CameraData(Camera, path, maskBuffer, projectionBuffer, maskCulling, projectionCulling);

            //Register it to our rendering path
            switch (path)
            {
                case RenderingPath.Forward:
                    SetForward(Camera, data);
                    break;

                case RenderingPath.DeferredShading:
                    SetDeferred(Camera, data);
                    break;
            }

            //Store data
            cameraData[Camera] = data;
        }

        //Return our updated Camera Data
        return data;
    }
    #endregion
    #region Pool Data
    private static Dictionary<int, ProjectionPool> Pools;
    internal static ProjectionPool PoolFromInstance(PoolInstance Instance)
    {
        //Make sure we are initialized
        if (Pools == null) Pools = new Dictionary<int, ProjectionPool>();

        ProjectionPool pool;
        if (!Pools.TryGetValue(Instance.id, out pool))
        {
            pool = new ProjectionPool(Instance);
            Pools.Add(Instance.id, pool);
        }
        return pool;
    }

    /**
     * Returns a pool with the specified name, if it exists. If it doesn't, returns the default pool.
     * @param Title The title of the pool to be returned.
     */
    public static ProjectionPool GetPool(string Title)
    {
        //Check Settings for an ID
        for (int i = 0; i < Settings.pools.Length; i++)
        {
            if (system.settings.pools[i].title == Title)
            {
                return PoolFromInstance(system.settings.pools[i]);
            }
        }
        //No valid pool set up, log a Warning and return the default pool
        Debug.LogWarning("No valid pool with the title : " + Title + " found. Returning default pool");
        return PoolFromInstance(system.settings.pools[0]);
    }
    /**
     * Returns a pool with the specified ID, if it exists. If it doesn't, returns the default pool.
     * @param ID The ID of the pool to be returned.
     */
    public static ProjectionPool GetPool(int ID)
    {
        //Check Settings for an ID
        for (int i = 0; i < Settings.pools.Length; i++)
        {
            if (system.settings.pools[i].id == ID)
            {
                return PoolFromInstance(system.settings.pools[i]);
            }
        }
        //No valid pool set up, log a Warning and return the default pool
        Debug.LogWarning("No valid pool with the ID : " + ID + " found. Returning default pool");
        return PoolFromInstance(system.settings.pools[0]);
    }
    #endregion

    //Initialization
    private bool initialized = false;
    public static void Initialize()
    {
        if (!System.initialized)
        {
            //Initialize our cube mesh before rendering kicks in
            GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            System.cube = gameObject.GetComponent<MeshFilter>().sharedMesh;

            //Destroy the mesh primitive gameObject
            if (Application.isPlaying) GameObject.Destroy(gameObject);
            else GameObject.DestroyImmediate(gameObject);

            //Register our projection events to all cameras
            Camera.onPreCull += CullProjections;
            Camera.onPreRender += RenderProjections;

            //Now Initialized
            System.initialized = true;
        }
    }
    public static void Uninitialize(bool Forced = false)
    {
        if (Forced || System.initialized && system.projections.Count == 0)
        {
            //Deregister our projection events
            Camera.onPreCull -= CullProjections;
            Camera.onPreRender -= RenderProjections;

            //Iterate over our camera data
            foreach (var cb in cameraData)
            {
                //Remove command buffers from all cameras in the dictionary
                if (cb.Key != null)
                {
                    switch (cb.Value.path)
                    {
                        case RenderingPath.Forward:
                            cb.Key.RemoveCommandBuffer(ForwardMaskEvent, cb.Value.maskBuffer);
                            break;
                        case RenderingPath.DeferredShading:
                            cb.Key.RemoveCommandBuffer(DeferredMaskEvent, cb.Value.maskBuffer);
                            cb.Key.RemoveCommandBuffer(DeferredProjectionEvent, cb.Value.projectionBuffer);
                            break;
                    }
                }
                //Dispose of all culling groups
                if (cb.Value.maskCulling != null)
                {
                    cb.Value.maskCulling.Dispose();
                    cb.Value.maskCulling = null;
                }
                if (cb.Value.projectionCulling != null)
                {
                    cb.Value.projectionCulling.Dispose();
                    cb.Value.maskCulling = null;
                }
            }

            //Clear camera Data
            cameraData.Clear();

            //No longer Initialized
            System.initialized = false;
        }
    }

    //Per Frame Updates
    private bool updated;

    //Primary Methods
    private static void CullProjections(Camera Camera)
    {
        //Grab our camera data
        CameraData data = GetData(Camera);

        //Check if valid
        if (data != null)
        {
            //Update the system before culling (once per frame, not per camera)
            if (System.initialized && !System.updated)
            {
                //Update our Pools
                if (Pools != null && Pools.Count > 0)
                {
                    for (int i = 0; i < Pools.Count; i++)
                    {
                        Pools.ElementAt(i).Value.Update(Time.deltaTime);
                    }
                }

                //Check our Rendering Path
                CheckRenderingPath();

                //If we are in forward rendering we need to make sure we are using the correct depth texture mode. 
                //Other assets might try and change it, or it could have been restored to the original prior to the scene being saved.
                if (data.path == RenderingPath.Forward) SetDepthTextureMode(Camera, data);

                //Reorder our projections
                ReorderProjections();

                //Update our projections
                UpdateProjections();

                //Update Culling Spheres
                RequestCullUpdate();

                //System has been updated
                System.updated = true;
            }
        
            //Set mask bounding spheres
            if (System.initialized && system.masks != null && system.masks.Count > 0)
            {
                data.maskCulling.SetBoundingSpheres(system.maskSpheres);
            }
            //Set projection culling spheres
            if (System.initialized && RenderingPath == RenderingPath.DeferredShading && system.projections != null && system.projections.Count > 0)
            {
                data.projectionCulling.SetBoundingSpheres(system.projectionSpheres);
            }
        }
    }
    private static void RenderProjections(Camera Camera)
    {
        //Reset the system to be update next frame
        System.updated = false;

        //Grab our camera data
        CameraData data = GetData(Camera);

        //Check if valid
        if (data != null)
        {
            //Adjust our scene clipping planes
            if (data.sceneCamera && Camera.farClipPlane > 1000)
            {
                LockClippingPlanes();
            }
            //Scene Focus Warning
            if (!system.sceneFocus && data.sceneCamera && Camera.farClipPlane > 100000)
            {
                Debug.LogWarning("Scene Camera Far Clipping Plane too far out - Projections in the scene view may appear strange or not at all. To fix this, simply focus on an object with a reasonable scale (Select then F key), or scroll in with the mouse wheel. This occurs as Unity sets its far clipping plane absurdly high when focusing large objects, or scrolling out with the mouse-wheel. As the depthbuffer is stored as a value between the near and far clipping planes, when the far clipping plane is set to high the depthbuffer becomes very inprecise. As a result, the position and Uv's of our projections also become very inprecise.");
                system.sceneFocus = true;
            }
            //Camera far clipping plane warning
            if (!system.cameraClipping && !data.sceneCamera && !data.previewCamera && Camera.farClipPlane > 1000000)
            {
                Debug.LogWarning("Cameras far clipping plane is too high to maintain an accurate Depth Buffer - Projections may appear strange or not at all. You'll also have a host of other issues, z-fighting among your objects etc.");
                system.cameraClipping = true;
            }
            //Make sure we are still in the right rendering path
            UpdateRenderingPath(Camera, data);

            //Update out mask buffer
            UpdateMaskBuffer(Camera, data);

            //Update our projection buffer
            UpdateProjectionBuffer(Camera, data);
        }
    }

    //Culling
    internal static void RequestCullUpdate()
    {
        //Masks
        if (system.masks != null && system.masks.Count > 0)
        {
            //Initialize || Resize array as necessary
            if (system.maskSpheres == null || system.maskSpheres.Length < system.masks.Count)
            {
                system.maskSpheres = new BoundingSphere[system.masks.Count * 2];
            }
            //Update array
            for (int i = 0; i < system.masks.Count; i++)
            {
                if (system.masks[i].Enabled)
                {
                    //Cache bounds
                    Bounds bounds = system.masks[i].Bounds;

                    //Update projections culling
                    system.maskSpheres[i].position = bounds.center;
                    system.maskSpheres[i].radius = Mathf.Max(bounds.size.x, Mathf.Max(bounds.size.y, bounds.size.z)) * 1.5f;
                }
            }
        }

        //Projections - Only required in deferred, unity handles culling in forward
        if (RenderingPath == RenderingPath.DeferredShading && system.projections != null && system.projections.Count > 0)
        {
            //Initialize || Resize array as necessary
            if (system.projectionSpheres == null || system.projectionSpheres.Length < system.projections.Count)
            {
                system.projectionSpheres = new BoundingSphere[system.projections.Count * 2];
            }

            //Update array & Cache cull position
            for (int i = 0; i < system.projections.Count; i++)
            {
                //Cache
                Transform projection = system.projections[i].transform;
                Vector3 scale = projection.lossyScale;

                //Update projections culling
                system.projectionSpheres[i].position = projection.position;
                system.projectionSpheres[i].radius = Mathf.Max(scale.x, Mathf.Max(scale.y, scale.z));
            }
        }
    }

    //Mask Buffer
    private static void UpdateMaskBuffer(Camera Camera, CameraData Data)
    {
        //Clear buffer
        Data.maskBuffer.Clear();

        switch (Camera.actualRenderingPath)
        {
            //Forward Rendering Path
            case RenderingPath.Forward:

                //Draw our masks
                DrawMasksForward(Camera, Data.maskBuffer, Data.maskCulling);
                break;

            //Deferred Rendering Path
            case RenderingPath.DeferredShading:

                //Draw our masks
                DrawMasksDeferrred(Camera, Data.maskBuffer, Data.maskCulling);
                break;
        }
    }
    private static void DrawMasksForward(Camera Camera, CommandBuffer Buffer, CullingGroup MasksCullingGroup)
    {
        //Create mask render texture
        int MaskBuffer = Shader.PropertyToID("_MaskBuffer");
        Buffer.GetTemporaryRT(MaskBuffer, -1, -1, 8);

        //Set as render target
        Buffer.SetRenderTarget(MaskBuffer, BuiltinRenderTextureType.CurrentActive);

        //Fill buffer with black
        Buffer.DrawMesh(Cube, Camera.transform.localToWorldMatrix, Mat_Mask, 0, 0);

        //Grab out masks
        List<ProjectionMask> masks = system.masks;

        //Fill the mask render texture
        if (masks != null && masks.Count > 0)
        {
            for (int i = 0; i < masks.Count; i++)
            {
                try { if (MasksCullingGroup.IsVisible(i)) DrawMask(Camera, Buffer, masks[i]); }
                catch (System.IndexOutOfRangeException) { DrawMask(Camera, Buffer, masks[i]); }
            }
        }

        //Release mask render texture so projection shader can access
        Buffer.ReleaseTemporaryRT(MaskBuffer);
    }
    private static void DrawMasksDeferrred(Camera Camera, CommandBuffer Buffer, CullingGroup MasksCullingGroup)
    {
        //Create mask render texture
        int MaskBuffer = Shader.PropertyToID("_MaskBuffer");

        Buffer.GetTemporaryRT(MaskBuffer, -1, -1);

        //Set as render target
        Buffer.SetRenderTarget(MaskBuffer, BuiltinRenderTextureType.CameraTarget);

        //Fill buffer with black
        Buffer.DrawMesh(Cube, Camera.transform.localToWorldMatrix, Mat_Mask, 0, 0);

        //Grab out masks
        List<ProjectionMask> masks = system.masks;

        //Fill the mask render texture
        if (masks != null && masks.Count > 0)
        {
            for (int i = 0; i < masks.Count; i++)
            {
                try { if (MasksCullingGroup.IsVisible(i)) DrawMask(Camera, Buffer, masks[i]); }
                catch (System.IndexOutOfRangeException) { DrawMask(Camera, Buffer, masks[i]); }
            }
        }

        //Release mask render texture so projection shader can access
        Buffer.ReleaseTemporaryRT(MaskBuffer);
    }
    private static void DrawMask(Camera Camera, CommandBuffer Buffer, ProjectionMask Mask)
    {
        //Check if our mask is enabled
        if (Mask.Enabled)
        {
            for (int i = 0; i < Mask.Mesh.subMeshCount; i++)
            {
                Buffer.DrawMesh(Mask.Mesh, Mask.transform.localToWorldMatrix, Mat_Mask, i, 0, Mask.Properties);
            }
        }
    }

    //Projection Buffer
    private static void UpdateProjectionBuffer(Camera Camera, CameraData Data)
    {
        //Clear buffer
        Data.projectionBuffer.Clear();

        //Grab our projections
        List<Projection> projections = system.projections;

        //Make sure we have projections to draw
        if (projections != null && projections.Count > 0)
        {
            switch (Data.path)
            {
                //Forward Rendering Path
                case RenderingPath.Forward:
                    //In forward rendering we let unity do all the rendering.
                    //Instead of using a command buffer we set up renderers on all our projection objects and let them render normally.
                    break;

                //Deferred Rendering Path
                case RenderingPath.DeferredShading:

                    //Blit in the screen before we start to change it - Static Pass
                    if (StaticPass) MultiChannelFullScreenBlit(Camera, Data.projectionBuffer);
                    else StaticNormalFullScreenBlit(Camera, Data.projectionBuffer);

                    //Iterate over all projections
                    for (int i = 0; i < projections.Count; i++)
                    {
                        //Draw using the command buffer
                        try
                        {
                            if (Data.projectionCulling.IsVisible(i))
                            {
                                //Draw the Projection
                                DrawDeferredProjection(Camera, Data.projectionBuffer, projections[i]);
                                //Set visibiility
                                projections[i].SetVisibility(true);
                            }
                            else
                            {
                                //Set visibiility
                                projections[i].SetVisibility(false);
                            }
                        }
                        catch (System.IndexOutOfRangeException)
                        {
                            //Draw Projection
                            DrawDeferredProjection(Camera, Data.projectionBuffer, projections[i]);
                        }
                    }
                    break;
            }
        }
    }
    private static void DrawDeferredProjection(Camera Camera, CommandBuffer Buffer, Projection Projection)
    {
        //Only draw if enabled & valid
        if (Projection.isActiveAndEnabled && Projection.RenderMaterial != null)
        {
            if (Projection.DeferredBuffers != null && Projection.DeferredBuffers.Length > 0)
            {
                //If the projection requires a pre-pass (usually for blended transparency), blit the buffers in there current state into the shader
                if (Projection.DeferredPrePass) MultiChannelBlit(Camera, Buffer, Projection);

                //Set the render targets of our buffer
                if (Camera.allowHDR)
                {
                    //Set to HDR rendertargets
                    Buffer.SetRenderTarget(Projection.DeferredHDRTargets, BuiltinRenderTextureType.CameraTarget);
                }
                else
                {
                    //Set to normal rendertargets
                    Buffer.SetRenderTarget(Projection.DeferredTargets, BuiltinRenderTextureType.CameraTarget);
                }

                //Draw our Projection
                Buffer.DrawMesh(Cube, Projection.RenderMatrix, Projection.RenderMaterial, 0, Projection.DeferredPass, Projection.MaterialProperties);
            }
        }
    }

    #region Passes To Targets
    //Creating new arrays allocates memory, reuse cached ones instead
    private static RenderTargetIdentifier[] one = new RenderTargetIdentifier[1];
    private static RenderTargetIdentifier[] two = new RenderTargetIdentifier[2];
    private static RenderTargetIdentifier[] three = new RenderTargetIdentifier[3];
    private static RenderTargetIdentifier[] four = new RenderTargetIdentifier[4];

    public static RenderTargetIdentifier[] PassesToTargets(bool[] Channels, bool HDR)
    {
        //How large an array do we require
        int count = 0;
        if (Channels[0]) count += 2;
        if (Channels[1]) count++;
        if (Channels[2]) count++;

        //Determine the cached array to use (Creating temporary arrays allocates a ton of memory)
        RenderTargetIdentifier[] buffers = null;
        switch (count)
        {
            case 0: return null;
            case 1: buffers = one; break;
            case 2: buffers = two; break;
            case 3: buffers = three; break;
            case 4: buffers = four; break;
        }

        //Reset count
        count = 0;

        //Assign new targets
        if (Channels[0])
        {
            buffers[count] = BuiltinRenderTextureType.GBuffer0;
            count++;
        }

        if (Channels[1])
        {
            buffers[count] = BuiltinRenderTextureType.GBuffer1;
            count++;
        }
        if (Channels[2])
        {
            buffers[count] = BuiltinRenderTextureType.GBuffer2;
            count++;
        }
        if (Channels[0])
        {
            if (HDR)
            {
                buffers[count] = BuiltinRenderTextureType.CameraTarget;
                count++;
            }
            else
            {
                buffers[count] = BuiltinRenderTextureType.GBuffer3;
                count++;
            }
        }
        return buffers;
    }
    #endregion
    #region Blit
    private static void MultiChannelBlit(Camera Camera, CommandBuffer Buffer, Projection Projection)
    {
        int AlbedoBuffer = Shader.PropertyToID("_DynAlbedo");
        int AmbientBuffer = Shader.PropertyToID("_DynAmbient");
        int GlossBuffer = Shader.PropertyToID("_DynGloss");
        int NormalBuffer = Shader.PropertyToID("_DynNormal");

        //Create temporary render textures for each buffer
        if (Projection.DeferredBuffers[0])
        {
            Buffer.GetTemporaryRT(AlbedoBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB32);
            if (Camera.allowHDR) Buffer.GetTemporaryRT(AmbientBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB2101010);
            else Buffer.GetTemporaryRT(AmbientBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGBHalf);
        }
        if (Projection.DeferredBuffers[1]) Buffer.GetTemporaryRT(GlossBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB32);
        if (Projection.DeferredBuffers[2]) Buffer.GetTemporaryRT(NormalBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB2101010);

        //Draw to the temporary render textures
        if (Projection.DeferredBuffers[0] && Projection.DeferredBuffers[1] && Projection.DeferredBuffers[2])
        {
            four[0] = AlbedoBuffer; four[1] = GlossBuffer; four[2] = NormalBuffer; four[3] = AmbientBuffer;
            DrawPrePass(Camera, Buffer, four, 6, Projection);
        }
        else if (Projection.DeferredBuffers[1] && Projection.DeferredBuffers[2])
        {
            two[0] = GlossBuffer; two[1] = NormalBuffer;
            DrawPrePass(Camera, Buffer, two, 5, Projection);
        }
        else if (Projection.DeferredBuffers[0] && Projection.DeferredBuffers[2])
        {
            three[0] = AlbedoBuffer; three[1] = NormalBuffer; three[2] = AmbientBuffer;
            DrawPrePass(Camera, Buffer, three, 4, Projection);
        }
        else if (Projection.DeferredBuffers[0] && Projection.DeferredBuffers[1])
        {
            three[0] = AlbedoBuffer; three[1] = GlossBuffer; three[2] = AmbientBuffer;
            DrawPrePass(Camera, Buffer, three, 3, Projection);
        }
        else if (Projection.DeferredBuffers[2])
        {
            one[0] = NormalBuffer;
            DrawPrePass(Camera, Buffer, one, 2, Projection);
        }
        else if (Projection.DeferredBuffers[1])
        {
            one[0] = GlossBuffer;
            DrawPrePass(Camera, Buffer, one, 1, Projection);
        }
        else
        {
            two[0] = AlbedoBuffer; two[1] = AmbientBuffer;
            DrawPrePass(Camera, Buffer, two, 0, Projection);
        }

        //Release temporary render textures
        if (Projection.DeferredBuffers[0])
        {
            Buffer.ReleaseTemporaryRT(AlbedoBuffer);
            Buffer.ReleaseTemporaryRT(AmbientBuffer);
        }
        if (Projection.DeferredBuffers[1]) Buffer.ReleaseTemporaryRT(GlossBuffer);
        if (Projection.DeferredBuffers[2]) Buffer.ReleaseTemporaryRT(NormalBuffer);
    }
    private static void DrawPrePass(Camera Camera, CommandBuffer Buffer, RenderTargetIdentifier[] Buffers, int Pass, Projection Projection)
    {
        //Copies the gbuffer values to render textures only where the projection is present.
        if (Buffers.Length > 0)
        {
            //Set Render Targets
            Buffer.SetRenderTarget(Buffers, BuiltinRenderTextureType.CameraTarget);

            //Draw
            Buffer.DrawMesh(Cube, Projection.RenderMatrix, Mat_DeferredBlit, 0, Pass);
        }
    }

    private static void StaticNormalFullScreenBlit(Camera Camera, CommandBuffer Buffer)
    {
        //Blit in the static normal (Required for correct normal generation)
        int NormalBuffer = Shader.PropertyToID("_StcNormal");

        //Create temporary render texture
        Buffer.GetTemporaryRT(NormalBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB2101010);

        //Set Render Targets
        Buffer.SetRenderTarget(NormalBuffer, BuiltinRenderTextureType.CameraTarget);

        //Draw
        Buffer.DrawMesh(Cube, Camera.transform.localToWorldMatrix, Mat_DeferredBlit, 0, 2);

        //Release
        Buffer.ReleaseTemporaryRT(NormalBuffer);
    }

    private static void MultiChannelFullScreenBlit(Camera Camera, CommandBuffer Buffer)
    {
        //Blit in the static normal (Required for correct normal generation)
        int AlbedoBuffer = Shader.PropertyToID("_StcAlbedo");
        int GlossBuffer = Shader.PropertyToID("_StcGloss");
        int NormalBuffer = Shader.PropertyToID("_StcNormal");
        int AmbientBuffer = Shader.PropertyToID("_StcAmbient");

        Buffer.GetTemporaryRT(AlbedoBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB32);
        Buffer.GetTemporaryRT(GlossBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB32);
        Buffer.GetTemporaryRT(NormalBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB2101010);
        if (Camera.allowHDR) Buffer.GetTemporaryRT(AmbientBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB2101010);
        else Buffer.GetTemporaryRT(AmbientBuffer, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGBHalf);

        four[0] = AlbedoBuffer; four[1] = GlossBuffer; four[2] = NormalBuffer; four[3] = AmbientBuffer;
        FullPrePass(Camera, Buffer, four);

        Buffer.ReleaseTemporaryRT(AlbedoBuffer);
        Buffer.ReleaseTemporaryRT(GlossBuffer);
        Buffer.ReleaseTemporaryRT(NormalBuffer);
        Buffer.ReleaseTemporaryRT(AmbientBuffer);
    }
    private static void FullPrePass(Camera Camera, CommandBuffer Buffer, RenderTargetIdentifier[] Buffers)
    {
        //Set Render Targets
        Buffer.SetRenderTarget(Buffers, BuiltinRenderTextureType.CameraTarget);

        //Draw
        Buffer.DrawMesh(Cube, Camera.transform.localToWorldMatrix, Mat_DeferredBlit, 0, 6);
    }
    #endregion
}

public enum SystemRenderingPath { Auto, Forward, Deferred }
public enum ProjectionType { Decal, Eraser, Pulse }

/**
* In-built projection pooling class. Use DynamicDecal.GetPool() to get a reference to a pool instance or use ProjectionPool.Default to get a reference to the default pool.
* You can the request projections from the pool as you see fit. Once you are done with them, instead of deleting them, use the Return method to return them back to the pool.
*/
public class ProjectionPool
{
    //Get Pool
    /**
     * Returns a pool with the specified name, if it exists. If it doesn't, returns the default pool.
     * @param Title The title of the pool to be returned.
     */
    public static ProjectionPool GetPool(string Title)
    {
        return DynamicDecals.GetPool(Title);
    }
    /**
     * Returns a pool with the specified ID, if it exists. If it doesn't, returns the default pool.
     * @param ID The ID of the pool to be returned.
     */
    public static ProjectionPool GetPool(int ID)
    {
        return DynamicDecals.GetPool(ID);
    }

    /**
    * Returns the default pool.
    */
    public static ProjectionPool Default
    {
        get
        {
            if (defaultPool == null) { defaultPool = DynamicDecals.PoolFromInstance(DynamicDecals.Settings.pools[0]); }
            return defaultPool;
        }
    }
    private static ProjectionPool defaultPool;
    
    //Constructor
    public ProjectionPool(PoolInstance Instance)
    {
        instance = Instance;
    }

    //Pool Details
    public string Title
    {
        get { return instance.title; }
    }
    public int ID
    {
        get { return instance.id; }
    }

    private PoolInstance instance;
    private int Limit
    {
        get
        {
            return instance.limits[QualitySettings.GetQualityLevel()];
        }
    }

    //Parent
    internal Transform Parent
    {
        get
        {
            if (parent == null)
            {
                parent = new GameObject(instance.title + " Pool").transform;
            }
            return parent;
        }
    }
    private Transform parent;
    
    //Pool
    private List<PoolItem> activePool;
    private List<PoolItem> inactivePool;

    //Update
    internal void Update(float DeltaTime)
    {
        //Update Pool Items
        if (activePool != null && activePool.Count > 0)
        {
            for (int i = activePool.Count - 1; i >= 0; i--)
            {
                //Remove deleted pool objects
                if (activePool[i].GameObject == null)
                {
                    activePool.RemoveAt(i);
                }
                else
                {
                    activePool[i].Update(DeltaTime);
                }
            }
        }
    }

    //Check Intersecting
    /**
    * Checks to see if a point is intersecting with any of the projections in the pool.
    * Returns true if an intersecting projection is found, otherwise returns false.
    * @param Point The type of projection being requested.
    * @param intersectionStrength How far within the bounds of the projection the point must be before it's considered an intersection. 0 will consider a point anywhere within a projections bounds as an intersections. 1 will only a point as intersecting if it is perfectly at the center of a projections bounds.
    */
    public bool CheckIntersecting(Vector3 Point, float intersectionStrength)
    {
        if (activePool != null && activePool.Count > 0)
        {
            for (int i = 0; i < activePool.Count; i++)
            {
                if (activePool[i].Projection.CheckIntersecting(Point) > intersectionStrength) return true;
            }
        }
        return false;
    }

    //Request
    /**
    * Returns a projection of the specified type from the pool.
    * Projection will be enabled and ready to use. Use the return method once your done with it, do not delete it.
    * @param Type The type of projection being requested.
    */
    public Projection Request(ProjectionType Type)
    {
        //Initialize active pool if required
        if (activePool == null) activePool = new List<PoolItem>();

        if (inactivePool != null && inactivePool.Count > 0)
        {
            //Grab the first item in the inactive pool
            PoolItem item = inactivePool[0];

            //Remove it from the inactive pool
            inactivePool.RemoveAt(0);

            //Add to the active pool
            activePool.Add(item);

            //Initialize pool item
            item.Reset(Type);

            return item.Projection;
        }
        else if (activePool.Count < Limit)
        {
            //Create our pool item
            PoolItem item = new PoolItem(this);

            //Initialize it
            item.Reset(Type);

            //Add it to the active pool
            activePool.Add(item);

            return item.Projection;
        }
        else
        {
            //Grab the oldest projection in the active pool
            PoolItem item = activePool[0];

            //Move it to the end of the pool
            activePool.RemoveAt(0);
            activePool.Add(item);

            //Initialize pool item
            item.Reset(Type);

            return item.Projection;
        }
    }

    /**
    * Returns a copy of the specified projection generated from the pool.
    * Projection will be enabled and ready to use. Use the return method once your done with it, do not delete it.
    * @param Projection The projection to copy. In 90% of use cases this should be a prefab.
    */
    public Projection RequestCopy(Projection Projection)
    {
        //Null Check
        if (Projection == null) return null;

        //Type Check
        if (Projection.GetType() == typeof(Decal))
        {
            Decal decal = (Decal)Request(ProjectionType.Decal);
            decal.CopyAllProperties((Decal)Projection);
            return decal;
        }
        if (Projection.GetType() == typeof(Eraser))
        {
            Eraser eraser = (Eraser)Request(ProjectionType.Eraser);
            eraser.CopyAllProperties((Eraser)Projection);
            return eraser;
        }
        if (Projection.GetType() == typeof(Pulse))
        {
            Pulse pulse = (Pulse)Request(ProjectionType.Pulse);
            pulse.CopyAllProperties((Pulse)Projection);
            return pulse;
        }

        throw new System.NotImplementedException("Projection Type not recognized, If your implementing your own projection types, you need to implement a copy method like the projection types above");
    }

    //Return
    /**
    * Returns the specified projection back to the pool. 
    * The projection will no longer be active or useable until it is requested from the pool again. All cached references to it should be nullified.
    * @param Projection The projection to return.
    */
    public void Return(Projection Projection)
    {
        if (Projection.PoolItem != null) Return(Projection.PoolItem);
    }
    internal void Return(PoolItem Item)
    {
        //Initialize if required
        if (inactivePool == null) inactivePool = new List<PoolItem>();

        //Remove it from the active pool
        activePool.Remove(Item);

        //Disable if still available (Projections can be deleted before items cleaned up on game end)
        if (Item.GameObject != null) Item.GameObject.SetActive(false);

        //Return projection to the inactive pool
        inactivePool.Add(Item);
    }
}
public class PoolItem
{
    //Pool
    public ProjectionPool Pool
    {
        get { return pool; }
    }
    private ProjectionPool pool;

    //GameObject
    public GameObject GameObject
    {
        get { return gameObject; }
    }
    private GameObject gameObject;

    //Projection
    public Projection Projection
    {
        get { return projection; }
    }
    private Projection projection;

    //Fade
    private FadeMethod fadeMethod;
    private float delay;
    private float inDuration;
    private float outDuration;

    //Culled
    private CullMethod cullMethod;
    private float cullDuration;

    //Tracking
    private float timeElapsed;
    private float timeSinceSeen;

    //Initializer
    public PoolItem(ProjectionPool Pool)
    {
        pool = Pool;
    }

    //Reset
    public void Reset(ProjectionType Type)
    {
        //Make sure we have a gameObject
        if (gameObject == null) gameObject = new GameObject("Projection");

        //Set parent
        gameObject.transform.SetParent(pool.Parent);
        
        //Make sure we are enabled
        gameObject.SetActive(true);

        //Just initialized, no time has elapsed yet
        timeElapsed = 0;

        //Reset fade properties
        fadeMethod = FadeMethod.None;
        inDuration = 0;
        delay = 0;
        outDuration = 0;

        //Reset culled properties
        cullMethod = CullMethod.None;
        timeSinceSeen = 0;
        cullDuration = 0;

        //Grab Projections
        Eraser eraser = gameObject.GetComponent<Eraser>();
        Pulse pulse = gameObject.GetComponent<Pulse>();
        Decal decal = gameObject.GetComponent<Decal>();

        //Disable other projections
        switch (Type)
        {
            case ProjectionType.Decal:
                if (eraser != null) eraser.enabled = false;
                if (pulse != null) pulse.enabled = false;
                if (decal == null) decal = gameObject.AddComponent<Decal>();
                projection = decal;
                break;
            case ProjectionType.Eraser:
                if (decal != null) decal.enabled = false;
                if (pulse != null) pulse.enabled = false;
                if (eraser == null) eraser = gameObject.AddComponent<Eraser>();
                projection = eraser;
                break;
            case ProjectionType.Pulse:
                if (decal != null) decal.enabled = false;
                if (eraser != null) eraser.enabled = false;
                if (pulse == null) pulse = gameObject.AddComponent<Pulse>();
                projection = pulse;
                break;
        }

        //Update reference
        projection.PoolItem = this;

        //Enable selected projection
        projection.enabled = true;

        //Reset
        projection.AlphaModifier = 1;
        projection.ScaleModifier = 1;
    }

    //Update (run every update on active poolitems)
    public void Update(float deltaTime)
    {
        //Fade
        if (fadeMethod != FadeMethod.None)
        {
            float fadeValue = 1;
            timeElapsed += deltaTime;

            //Calculate FadeValue
            if (timeElapsed < inDuration) fadeValue = (1 - ((inDuration - timeElapsed) / inDuration));
            if (timeElapsed > inDuration + delay) fadeValue = ((inDuration + delay + outDuration - timeElapsed) / outDuration);

            //Fade out the projection
            if (fadeMethod == FadeMethod.Alpha || fadeMethod == FadeMethod.Both) projection.AlphaModifier = fadeValue;
            if (fadeMethod == FadeMethod.Scale || fadeMethod == FadeMethod.Both) projection.ScaleModifier = fadeValue;

            if (timeElapsed >= (inDuration + delay + outDuration))
            {
                //Return this item to the pool
                pool.Return(this);
            }
        }
        else
        {
            projection.AlphaModifier = 1;
            projection.ScaleModifier = 1;
        }

        if (cullMethod != CullMethod.None)
        {
            if (projection.Visible)
            {
                timeSinceSeen = 0;
            }
            else
            {
                timeSinceSeen += deltaTime;
            }
                
            
            if (timeSinceSeen > cullDuration)
            {
                //Return this item to the pool
                pool.Return(this);
            }
        }
    }

    //Utility
    public void Fade(FadeMethod Method, float InDuration, float Delay, float OutDuration)
    {
        fadeMethod = Method;
        delay = Delay;
        inDuration = InDuration;
        outDuration = OutDuration;
    }
    public void Culled(CullMethod Method, float Duration)
    {
        cullMethod = Method;
        cullDuration = Duration;
    }
    public void Return()
    {
        pool.Return(this);
    }
}

public enum FadeMethod { None, Alpha, Scale, Both }
public enum CullMethod { None, Remove }