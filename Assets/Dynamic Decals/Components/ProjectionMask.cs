﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

/**
* The Mask Component. This component allows us to declare renderers as a part of one or more masking layers. Projections like decals, eraser or pulses can then choose to ignore that masking layer, or render only to that masking layer.
*/
[ExecuteInEditMode]
public class ProjectionMask : MonoBehaviour {

    public bool Enabled
    {
        get
        {
            #if UNITY_EDITOR
            if (PrefabUtility.GetPrefabType(this) != PrefabType.None) return true;
            #endif
            if (source != null) return source.enabled;
            else return false;
        }
    }
    /**
    * Mask Layer 1. 
    * Should this object be apart of the first masking layer? Objects can be apart of multiple masking layers.
    */
    public bool Layer1
    {
        get { return layers[0]; }
        set
        {
            layers[0] = value;
            changed = true;
        }
        
    }
    /**
    * Mask Layer 2. 
    * Should this object be apart of the second masking layer? Objects can be apart of multiple masking layers.
    */
    public bool Layer2
    {
        get { return layers[1]; }
        set
        {
            layers[1] = value;
            changed = true;
        }

    }
    /**
    * Mask Layer 3. 
    * Should this object be apart of the third masking layer? Objects can be apart of multiple masking layers.
    */
    public bool Layer3
    {
        get { return layers[2]; }
        set
        {
            layers[2] = value;
            changed = true;
        }

    }
    /**
    * Mask Layer 4. 
    * Should this object be apart of the fourth masking layer? Objects can be apart of multiple masking layers.
    */
    public bool Layer4
    {
        get { return layers[3]; }
        set
        {
            layers[3] = value;
            changed = true;
        }

    }

    public Mesh Mesh
    {
        get
        {
            if (skinned)
            {
                if (mesh == null) mesh = new Mesh();
                ((SkinnedMeshRenderer)source).BakeMesh(mesh);
                
            }
            return mesh;
        }
    }
    public Bounds Bounds
    {
        get { return (source != null) ? source.bounds : new Bounds(); }
    }

    [SerializeField]
    private bool[] layers = new bool[4];
    
    private Mesh mesh;
    private Bounds bounds;
    private Renderer source;
    private bool skinned;

    #region Material Properties
    public MaterialPropertyBlock Properties
    {
        get
        {
            UpdateProperties();
            return properties;
        }
    }
    private MaterialPropertyBlock properties;

    private bool changed = true;
    public void Update()
    {
        changed = true;
    }

    private void UpdateProperties()
    {
        if (properties == null) properties = new MaterialPropertyBlock();

        if (changed)
        {
            properties.Clear();
            properties.SetFloat("_Layer1", (layers[0]) ? 1 : 0);
            properties.SetFloat("_Layer2", (layers[1]) ? 1 : 0);
            properties.SetFloat("_Layer3", (layers[2]) ? 1 : 0);
            properties.SetFloat("_Layer4", (layers[3]) ? 1 : 0);
        }
        changed = false;
    }
    #endregion

    #region Generic Methods
    private void Start()
    {
        Initialize();
        Register();
    }
    private void OnEnable()
    {
        Initialize();
        Register();
    }
    private void OnDisable()
    {
        Deregister();
    }
    #endregion

    #region Initialization
    private void Initialize()
    {
        if (GetComponent<MeshRenderer>() != null)
        {
            //Cache our mesh
            mesh = GetComponent<MeshFilter>().sharedMesh;

            //Cache our renderer
            source = GetComponent<MeshRenderer>();
            skinned = false;
        }
        if (GetComponent<SkinnedMeshRenderer>() != null)
        {
            //Cache our renderer
            source = GetComponent<SkinnedMeshRenderer>();
            skinned = true;
        }
    }
    #endregion

    #region Registration
    private void Register()
    {
        DynamicDecals.AddMask(this);
    }
    private void Deregister()
    {
        DynamicDecals.RemoveMask(this);
    }
    #endregion
}
