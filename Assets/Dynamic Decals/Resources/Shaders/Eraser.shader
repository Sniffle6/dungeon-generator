﻿Shader "Decal/Eraser"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Multiplier("Multiplier", Range(0.0, 1.0)) = 1.0

		_Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5
		_NormalCutoff("Normal Cutoff", Range(0.0, 1.0)) = 0.5

		_MaskBase("Mask Base", Range(0.0, 1.0)) = 0.0
		_MaskLayers("Layers", Color) = (0.5, 0.5, 0.5, 0.5)
	}
	SubShader
	{
		Tags{ "Queue" = "AlphaTest+1" "DisableBatching" = "True"  "IgnoreProjector" = "True"}
		ZWrite Off ZTest Always Cull Front
		
		//Forward
		Pass
		{
			Name "FORWARD"
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma glsl

			#pragma vertex vertProjection
			#pragma fragment fragForward

			#pragma multi_compile _AlphaTest _Blend
			
			#include "Cginc/ForwardProjections.cginc"

			half4 fragForward(ProjectionInput i) : SV_Target
			{
				//Calculate screen space position
				float2 screenPos = i.screenPos.xy / i.screenPos.w;

				//Calculate depth & normals
				float depth;
				float3 surfaceNormal = DepthNormal(screenPos, depth);

				//Calculate world position
				float3 posWorld = CalculateWorldPos(i.ray, depth);

				//Calculate local uvs
				float2 localUVs = ProjectionUVs(posWorld);

				//Occlusion
				half occlusion = ShapeOcclusion(localUVs);

				//Clip
				ClipPixels(occlusion);
				ClipMasking(screenPos);
				ClipNormal(surfaceNormal, i.worldForward);

				//Output
				return EraserOutput(screenPos, occlusion);
			}
			ENDCG
		}

		//Deferred
		Pass
		{
			Name "DEFERRED"
			Tags{ "LightMode" = "Deferred" }

			CGPROGRAM
			#pragma target 3.0
			#pragma exclude_renderers nomrt
			#pragma glsl

			#pragma vertex vertProjection
			#pragma fragment fragDeferred

			#pragma multi_compile _AlphaTest _Blend
			
			#include "Cginc/DeferredProjections.cginc"

			void fragDeferred(ProjectionInput i, out half4 outAlbedo : COLOR0, out half4 outGloss : COLOR1, out half4 outNormal : COLOR2, out half4 outAmbient : COLOR3)
			{
				//Calculate screen space position
				float2 screenPos = i.screenPos.xy / i.screenPos.w;

				//Calculate depth
				float depth = Depth(screenPos);

				//Calculate world position
				float3 posWorld = CalculateWorldPos(i.ray, depth);

				//Calculate local uvs
				float2 localUVs = ProjectionUVs(posWorld);

				//Occlusion
				half occlusion = ShapeOcclusion(localUVs);

				//Clip
				ClipPixels(occlusion);
				ClipMasking(screenPos);
				ClipNormal(screenPos, i.worldForward);

				//Output
				outAlbedo = EraserAlbedo(screenPos, occlusion);
				outGloss = EraserGloss(screenPos, occlusion);
				outNormal = EraserNormal(screenPos, occlusion);
				outAmbient = EraserAmbient(screenPos, occlusion);
			}
			ENDCG
		}
	}
}