﻿Shader "Decal/Pulse"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)

		_Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

		_MaskBase("Mask Base", Range(0.0, 1.0)) = 0.0
		_MaskLayers("Layers", Color) = (0.5, 0.5, 0.5, 0.5)
	}
	SubShader
	{
		Tags{ "Queue" = "AlphaTest+1" "DisableBatching" = "True"  "IgnoreProjector" = "True" }
		ZWrite Off ZTest Always Cull Front

		//Forward
		Pass
		{
			Name "FORWARD"
			Tags{ "LightMode" = "ForwardBase"}
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog
			#pragma glsl

			#pragma shader_feature _ALPHATEST_ON

			#pragma vertex vertPulse
			#pragma fragment fragForward

			#include "Cginc/ForwardProjections.cginc"

			half4 fragForward(PulseInput i) : SV_Target
			{
				//Calculate screenspace position
				float2 screenPos = i.screenPos.xy / i.screenPos.w;

				//Calculate depth
				float depth;
				DepthNormal(screenPos, depth);

				//Calculate world position
				float3 posWorld = CalculateWorldPos(i.ray, depth);

				//Calculate local uvs
				float2 localUVs = PulseUVs(posWorld);

				//Calculate our occlusion
				half occlusion = AlbedoOcclusion(localUVs);

				//Clip Pixels
				ClipPixels(occlusion);
				ClipMasking(screenPos);

				half3 c = Albedo(localUVs);

				UNITY_APPLY_FOG(i.fogCoord, c);
				return Output(c, occlusion);
			}
			ENDCG
		}

		//Deferred
		Pass
		{
			Name "DEFERRED"
			Tags{ "LightMode" = "Deferred" }

			CGPROGRAM
			#pragma target 3.0
			#pragma exclude_renderers nomrt
			#pragma glsl

			#pragma vertex vertPulse
			#pragma fragment fragDeferred

			#pragma shader_feature _ALPHATEST_ON
			#pragma multi_compile ___ UNITY_HDR_ON

			#include "Cginc/DeferredProjections.cginc"

			void fragDeferred(PulseInput i, out half4 outAlbedo : COLOR0, out half4 outSmoothSpec : COLOR1, out half4 outEmission : COLOR2)
			{
				//Calculate screenspace position
				float2 screenPos = i.screenPos.xy / i.screenPos.w;

				//Calculate Depth
				float depth = Depth(screenPos);

				//Calculate world position
				float3 posWorld = CalculateWorldPos(i.ray, depth);

				//Calculate local uvs
				float2 localUVs = PulseUVs(posWorld);

				//Calculate our occlusion
				half occlusion = AlbedoOcclusion(localUVs);

				//Clip Pixels
				ClipPixels(occlusion);
				ClipMasking(screenPos);

				half3 c = Albedo(localUVs);

				//Albedo output
				outAlbedo = AlbedoOutput(c, occlusion, screenPos);
				//Specsmooth output
				half4 s = half4(0,0,0,0);
				outSmoothSpec = SpecSmoothOutput(s, occlusion, screenPos);
				//Emission output
				outEmission = EmissionOutput(half4(c, 1), occlusion, screenPos);
			}
			ENDCG
		}
	}
	Fallback Off
}